using System;
using UnityEngine;
using Sirenix.OdinInspector;

namespace GameJam.Gameplay.Character
{
    public abstract class CharacterAnimator<TState> : ValidatableMonoBehaviour
        where TState : Enum
    {
        [Header("Components")]
        [ValidateInput(nameof(ValidateObject), NullError)]
        [SerializeField] protected Animator _animator = null;


        private void OnValidate() =>
            _animator ??= GetComponent<Animator>();


        public void PlayState(TState state)
        {
            string triggerName = GetTrigger(state);
            _animator.SetTrigger(triggerName);
        }

        protected abstract string GetTrigger(TState state);
    }
}
