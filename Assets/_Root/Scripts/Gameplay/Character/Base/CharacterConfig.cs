using UnityEngine;
using Sirenix.OdinInspector;
using GameJam.Gameplay.Components.Speeding;

namespace GameJam.Gameplay.Character
{
    public class CharacterConfig : ValidatableMonoBehaviour
    {
        [field: SerializeField, HideLabel] public SpeedConfig Speed { get; private set; }
    }
}
