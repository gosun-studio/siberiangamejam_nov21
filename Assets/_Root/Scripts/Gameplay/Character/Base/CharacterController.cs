using System;
using UnityEngine;
using UnityEngine.Events;
using GameJam.Gameplay.Components.Movement;
using Object = UnityEngine.Object;

namespace GameJam.Gameplay.Character
{
    public class CharacterController<TView, TAnimator, TModel, TState> : IDisposable
        where TView : CharacterView<TAnimator, TState>
        where TAnimator : CharacterAnimator<TState>
        where TModel : CharacterModel
        where TState : Enum
    {
        public event UnityAction Started
        {
            add => _move.Started += value;
            remove => _move.Started -= value;
        }
        public event UnityAction Stopped
        {
            add => _move.Stopped += value;
            remove => _move.Stopped -= value;
        }
        public event UnityAction Completed
        {
            add => _move.Completed += value;
            remove => _move.Completed -= value;
        }

        protected readonly MoveComponent _move;
        protected readonly TView _view;
        protected readonly TModel _model;

        private bool _isDisposed;


        public CharacterController(MoveComponent move, TView view, TModel model)
        {
            _move = move;
            _view = view;
            _model = model;
            Subscribe();
        }

        public void Dispose()
        {
            if (_isDisposed)
                return;

            _isDisposed = true;
            Unsubscribe();

            OnDispose();
            _move.Dispose();
            Object.Destroy(_view.gameObject);
        }


        private void Subscribe()
        {
            _move.Started += OnMoveStarted;
            _move.Stopped += OnMoveStopped;
            _move.Completed += OnMoveCompleted;
            _move.TargetChanged += OnTargetChanged;
        }

        private void Unsubscribe()
        {
            _move.Started -= OnMoveStarted;
            _move.Stopped -= OnMoveStopped;
            _move.Completed -= OnMoveCompleted;
            _move.TargetChanged -= OnTargetChanged;
        }

        protected virtual void OnDispose() { }
        protected virtual void OnMoveStarted() { }
        protected virtual void OnMoveStopped() { }
        protected virtual void OnMoveCompleted() { }
        protected virtual void OnTargetChanged() { }


        public void Move(Vector3 position) => _move.Start(position);
        public void Move() => _move.Start();
        public void Stop() => _move.Stop();
    }
}
