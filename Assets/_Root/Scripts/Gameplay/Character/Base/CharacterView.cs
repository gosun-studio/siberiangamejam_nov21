using System;
using UnityEngine;
using Sirenix.OdinInspector;

namespace GameJam.Gameplay.Character
{
    [RequireComponent(typeof(CharacterConfig))]
    public class CharacterView<TAnimator, TState> : ValidatableMonoBehaviour
        where TAnimator : CharacterAnimator<TState>
        where TState : Enum
    {
        [Header("Components")]
        [ValidateInput(nameof(ValidateObject), NullError)]
        [SerializeField] protected TAnimator _animator = null;

        private void OnValidate() =>
            _animator ??= GetComponent<TAnimator>();

        public void PlayState(TState state) =>
            _animator.PlayState(state);
    }
}
