using UnityEngine;
using System.ComponentModel;

namespace GameJam.Gameplay.Character.Cat
{
    public class CatAnimator : CharacterAnimator<CatState>
    {
        [SerializeField] private string IdleTrigger = "";
        [SerializeField] private string MovingTrigger = "";
        [SerializeField] private string MovingBlend = "";


        public void SetBlend(float value) =>
            _animator.SetFloat(MovingBlend, value);

        protected override string GetTrigger(CatState state) =>
            state switch
            {
                CatState.Idle => IdleTrigger,
                CatState.Moving => MovingTrigger,
                _ => throw new InvalidEnumArgumentException()
            };
    }
}
