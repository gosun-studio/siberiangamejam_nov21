using UnityEngine;
using GameJam.Gameplay.Components.Clicking;

namespace GameJam.Gameplay.Character.Cat
{
    public class CatClickingDirector
    {
        private readonly CatController _catController;
        private readonly MovingPlaneClickDetector _clickDetector;

        private bool _isDispose;


        public CatClickingDirector(CatController catController, MovingPlaneClickDetector clickDetector)
        {
            _catController = catController;
            _clickDetector = clickDetector;
            Subscribe();
        }

        public void Dispose()
        {
            if (_isDispose)
                return;

            _isDispose = true;
            Unsubscribe();
        }


        private void Subscribe() => _clickDetector.ClickDetected += OnClickDetected;
        private void Unsubscribe() => _clickDetector.ClickDetected -= OnClickDetected;

        private void OnClickDetected(Vector3 position) => _catController.Move(position);
    }
}
