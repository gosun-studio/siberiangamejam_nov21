using GameJam.Gameplay.Components.Movement;

namespace GameJam.Gameplay.Character.Cat
{
    public class CatController : CharacterController<CatView, CatAnimator, CatModel, CatState>
    {
        private float CurrentBlendValue => _move.Direction.x < 0 ? -1 : 1;

        public CatController(MoveComponent move, CatView view, CatModel model) : base(move, view, model)
        { }

       protected override void OnMoveStarted() => _view.PlayState(CatState.Moving);
       protected override void OnMoveStopped() => _view.PlayState(CatState.Idle);
       protected override void OnTargetChanged() => _view.SetBlend(CurrentBlendValue);
    }
}
