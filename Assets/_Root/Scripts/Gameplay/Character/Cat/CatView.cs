using UnityEngine;

namespace GameJam.Gameplay.Character.Cat
{
    [RequireComponent(typeof(CatConfig))]
    public class CatView : CharacterView<CatAnimator, CatState>
    {
        public void SetBlend(float value) => _animator.SetBlend(value);
    }
}
