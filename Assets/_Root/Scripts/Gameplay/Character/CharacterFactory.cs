using UnityEngine;
using GameJam.Gameplay.Character.Cat;
using GameJam.Gameplay.Character.Player;
using GameJam.Gameplay.Components.Timing;
using GameJam.Gameplay.Components.Movement;
using GameJam.Gameplay.Components.Speeding;

namespace GameJam.Gameplay.Character
{
    public class CharacterFactory
    {
        private readonly SpeedCalculatorFactory _speedCalculatorFactory;


        public CharacterFactory(SpeedCalculatorFactory speedCalculatorFactory) =>
            _speedCalculatorFactory = speedCalculatorFactory;


        public PlayerController Create(PlayerConfig config, PlayerView view)
        {
            var model = new PlayerModel();
            MoveComponent moveComponent = CreateMoveComponent(config.Speed, view);

            return new PlayerController(moveComponent, view, model);
        }

        public CatController Create(CatConfig config, CatView view)
        {
            var model = new CatModel();
            MoveComponent moveComponent = CreateMoveComponent(config.Speed, view);

            return new CatController(moveComponent, view, model);
        }


        private MoveComponent CreateMoveComponent(SpeedConfig speedConfig, MonoBehaviour view) =>
            new MoveComponent
            (
                view.transform,
                new StopwatchComponent(view),
                _speedCalculatorFactory.Create(speedConfig)
            );
    }
}
