using UnityEngine;
using System.ComponentModel;

namespace GameJam.Gameplay.Character.Player
{
    public class PlayerAnimator : CharacterAnimator<PlayerState>
    {
        [SerializeField] private string IdleTrigger = "";
        [SerializeField] private string MovingTrigger = "";
        [SerializeField] private string MoveBlendX = "";
        [SerializeField] private string MoveBlendY = "";


        public void SetBlend(Vector2 value)
        {
            _animator.SetFloat(MoveBlendX, value.x);
            _animator.SetFloat(MoveBlendY, value.y);
        }

        protected override string GetTrigger(PlayerState state) =>
            state switch
            {
                PlayerState.Idle => IdleTrigger,
                PlayerState.Moving => MovingTrigger,
                _ => throw new InvalidEnumArgumentException()
            };
    }
}
