using UnityEngine;
using GameJam.Gameplay.Components.Movement;

namespace GameJam.Gameplay.Character.Player
{
    public class PlayerController : CharacterController<PlayerView, PlayerAnimator, PlayerModel, PlayerState>
    {
        private Vector2 CurrentBlendValue => _move.Direction.normalized;

        public Vector3 Position => _view.transform.position;

        public PlayerController(MoveComponent move, PlayerView view, PlayerModel model) : base(move, view, model)
        { }

        protected override void OnMoveStarted() => _view.PlayState(PlayerState.Moving);
        protected override void OnMoveStopped() => _view.PlayState(PlayerState.Idle);
        protected override void OnTargetChanged() => _view.SetBlend(CurrentBlendValue);
    }
}
