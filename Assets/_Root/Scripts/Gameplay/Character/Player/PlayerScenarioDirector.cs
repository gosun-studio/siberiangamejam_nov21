using UnityEngine;
using UnityEngine.Events;
using GameJam.Gameplay.Paths;

namespace GameJam.Gameplay.Character.Player
{
    public class PlayerScenarioDirector
    {
        public event UnityAction PathCompleted;

        private readonly PlayerController _player;
        private readonly PathProgress _pathProgress;
        private readonly PathsDirector _pathsDirector;

        private bool _isDispoose;


        public PlayerScenarioDirector(PlayerController player, PathsDirector pathsDirector)
        {
            _player = player;
            _pathsDirector = pathsDirector;
            _pathProgress = _pathsDirector.GetPathProgress(_player.Position);
            Subscribe();
        }

        public void Dispose()
        {
            if (_isDispoose)
                return;

            _isDispoose = true;
            Unsubscribe();
        }


        private void Subscribe()
        {
            _player.Completed += OnMoveCompleted;
            _pathProgress.PathCompleted += OnPathCompleted;
            _pathProgress.PointCompleted += OnPointCompleted;
        }

        private void Unsubscribe()
        {
            _player.Completed -= OnMoveCompleted;
            _pathProgress.PathCompleted -= OnPathCompleted;
            _pathProgress.PointCompleted -= OnPointCompleted;
        }


        private void OnMoveCompleted() => _pathProgress.CompletePoint();
        private void OnPathCompleted() => PathCompleted?.Invoke();
        private void OnPointCompleted() => Start();


        public void Start()
        {
            if (_pathProgress.IsCompleted)
                return;

            Vector3 currentPoint = _pathProgress.CurrentPoint;
            _player.Move(currentPoint);
        }

        public void Stop() => _player.Stop();
    }
}
