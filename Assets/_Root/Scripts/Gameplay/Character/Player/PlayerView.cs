using UnityEngine;

namespace GameJam.Gameplay.Character.Player
{
    [RequireComponent(typeof(PlayerConfig))]
    public class PlayerView : CharacterView<PlayerAnimator, PlayerState>
    {
        public void SetBlend(Vector2 value) => _animator.SetBlend(value);
    }
}
