using System;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace GameJam.Gameplay.Components.Catching
{
    public class ObjectCatcher<TComponent> : ValidatableMonoBehaviour
    {
        [Flags]
        private enum ColliderType
        {
            None = 0,
            Collision = 1 << 0,
            Collision2D = 1 << 1,
            Trigger = 1 << 2,
            Trigger2D = 1 << 3
        }

        [Header("Components")]
        [ValidateInput(nameof(ValidateObject), NullError)]
        [SerializeField] private Collider _collider = null;

        [Header("Settings")]
        [SerializeField] private ColliderType Type = ColliderType.None;

        [Header("Events")]
        [SerializeField] private UnityEvent<TComponent> _caught = new UnityEvent<TComponent>();
        [SerializeField] private UnityEvent<TComponent> _missed = new UnityEvent<TComponent>();

        public event UnityAction<TComponent> Caught
        {
            add => _caught.AddListener(value);
            remove => _caught.RemoveListener(value);
        }

        public event UnityAction<TComponent> Missed
        {
            add => _missed.AddListener(value);
            remove => _missed.RemoveListener(value);
        }


        private void OnEnable() => enabled = true;
        private void OnDisable() => enabled = false;
        private void OnValidate() => _collider ??= GetComponent<Collider>();
        private void OnDestroy()
        {
            _caught.RemoveAllListeners();
            _missed.RemoveAllListeners();
        }


        private void OnTriggerEnter(Collider other) => TryCatch(ColliderType.Trigger, other.gameObject);
        private void OnTriggerEnter2D(Collider2D other) => TryCatch(ColliderType.Trigger2D, other.gameObject);
        private void OnCollisionEnter(Collision other) => TryCatch(ColliderType.Collision, other.gameObject);
        private void OnCollisionEnter2D(Collision2D other) => TryCatch(ColliderType.Collision2D, other.gameObject);

        private void OnTriggerExit(Collider other) => TryMiss(ColliderType.Trigger, other.gameObject);
        private void OnTriggerExit2D(Collider2D other) => TryMiss(ColliderType.Trigger2D, other.gameObject);
        private void OnCollisionExit(Collision other) => TryMiss(ColliderType.Collision, other.gameObject);
        private void OnCollisionExit2D(Collision2D other) => TryMiss(ColliderType.Collision2D, other.gameObject);


        private bool TryCatch(ColliderType colliderType, GameObject gameObject) =>
            CanHandle(colliderType) && TryCatch(gameObject);

        private bool TryMiss(ColliderType colliderType, GameObject gameObject) =>
            CanHandle(colliderType) && TryMiss(gameObject);

        private bool CanHandle(ColliderType colliderType) => enabled && Type.HasFlag(colliderType);


        private bool TryCatch(GameObject gameObject) => TryHandle(gameObject, _caught);

        private bool TryMiss(GameObject gameObject) => TryHandle(gameObject, _missed);

        private bool TryHandle(GameObject gameObject, UnityEvent<TComponent> onHandled)
        {
            if (TryGetTargetComponent(gameObject, out TComponent component) == false)
                return false;

            onHandled?.Invoke(component);
            return true;
        }

        private bool TryGetTargetComponent(GameObject gameObject, out TComponent targetComponent)
        {
            if (gameObject == null)
            {
                targetComponent = default;
                return false;
            }

            targetComponent = gameObject.GetComponent<TComponent>() ?? gameObject.GetComponentInParent<TComponent>(true);
            return targetComponent != null;
        }
    }
}
