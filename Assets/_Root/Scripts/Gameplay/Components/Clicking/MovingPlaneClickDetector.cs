using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;
using GameJam.Systems.Clicking;
using GameJam.Gameplay.Components.Movement;

namespace GameJam.Gameplay.Components.Clicking
{
    public class MovingPlaneClickDetector : ValidatableMonoBehaviour
    {
        [Header("Components")]
        [ValidateInput(nameof(ValidateObject), NullError)]
        [SerializeField] private ObjectClicker _clicker = null;

        [Header("Events")]
        [SerializeField] private UnityEvent<Vector3> _clickDetected = new UnityEvent<Vector3>();

        public event UnityAction<Vector3> ClickDetected
        {
            add => _clickDetected.AddListener(value);
            remove => _clickDetected.RemoveListener(value);
        }


        private void OnEnable() => _clicker.Clicked += OnClicked;
        private void OnDisable() => _clicker.Clicked -= OnClicked;
        private void OnDestroy() => _clickDetected.RemoveAllListeners();


        private void OnClicked(RaycastHit hit)
        {
            if (hit.transform == null)
                return;

            if (hit.transform.TryGetComponent<MovingPlane>(out var plane))
                _clickDetected?.Invoke(hit.point);
        }
    }
}
