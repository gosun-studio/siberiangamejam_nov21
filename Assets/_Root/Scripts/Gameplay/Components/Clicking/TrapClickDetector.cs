using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;
using GameJam.Gameplay.Trap;
using GameJam.Systems.Clicking;

namespace GameJam.Gameplay.Components.Clicking
{
    public class TrapClickDetector : ValidatableMonoBehaviour
    {
        [Header("Components")]
        [ValidateInput(nameof(ValidateObject), NullError)]
        [SerializeField] private ObjectClicker _clicker = null;

        [Header("Events")]
        [SerializeField] private UnityEvent<TrapView> _clickDetected = new UnityEvent<TrapView>();

        public event UnityAction<TrapView> ClickDetected
        {
            add => _clickDetected.AddListener(value);
            remove => _clickDetected.RemoveListener(value);
        }


        private void OnEnable() => _clicker.Clicked += OnClicked;
        private void OnDisable() => _clicker.Clicked -= OnClicked;
        private void OnDestroy() => _clickDetected.RemoveAllListeners();


        private void OnClicked(RaycastHit hit)
        {
            if (hit.transform == null)
                return;

            var trapView = hit.transform.GetComponentInParent<TrapView>();
            if (trapView)
                _clickDetected?.Invoke(trapView);
        }
    }
}
