﻿using System;
using UnityEngine;
using UnityEngine.Events;
using GameJam.Gameplay.Components.Timing;
using GameJam.Gameplay.Components.Speeding;

namespace GameJam.Gameplay.Components.Movement
{
    public class MoveComponent : IDisposable
    {
        public event UnityAction Started;
        public event UnityAction Stopped;
        public event UnityAction Completed;
        public event UnityAction TargetChanged;

        private readonly Transform _transform;
        private readonly StopwatchComponent _stopwatch;
        private readonly SpeedCalculator _speedCalculator;

        private bool _isDisposed;
        private Vector3 _targetPosition;

        private Vector3 Position
        {
            get => _transform.position;
            set => _transform.position = value;
        }

        public Vector3 Direction { get; private set; }
        public bool IsMoving => _stopwatch.isCounting;


        public MoveComponent(Transform transform, StopwatchComponent stopwatch, SpeedCalculator speedCalculator)
        {
            _transform = transform;
            _stopwatch = stopwatch;
            _speedCalculator = speedCalculator;

            Direction = Vector3.zero;
            _targetPosition = Vector3.zero;

            Subscribe();
        }

        public void Dispose()
        {
            if (_isDisposed)
                return;

            _isDisposed = true;
            Stop();
            Unsubscribe();

            _stopwatch.Dispose();
        }

        private void Subscribe() =>
            _stopwatch.Updated += OnUpdate;

        private void Unsubscribe() =>
            _stopwatch.Updated -= OnUpdate;


        public void Start(Vector3 targetPosition)
        {
            _targetPosition = targetPosition;
            Direction = CalcActualDirection();
            Start();
        }

        public void Start()
        {
            if (IsMoving)
            {
                _stopwatch.StopCounting();
                _stopwatch.StartCounting();
            }
            else
            {
                _stopwatch.StartCounting();
                Started?.Invoke();
            }

            TargetChanged?.Invoke();
        }

        public void Stop()
        {
            if (!IsMoving)
                return;

            _stopwatch.StopCounting();
            Stopped?.Invoke();
        }

        private void Complete()
        {
            if (!IsMoving)
                return;

            Stop();
            Completed?.Invoke();
        }

        private void OnUpdate(float dt)
        {
            float speed = _speedCalculator.GetCurrent(_stopwatch.CurrentTime);
            Position += Direction * (speed * dt);

            if (IsDistanceOvercome() || IsDistanceCompleted())
                Complete();
        }

        private bool IsDistanceCompleted() =>
            Mathf.Approximately(CalcActualSquaredDistance(), default);

        private bool IsDistanceOvercome()
        {
            Vector3 actualDirection = CalcActualDirection();
            float directionAngle = Vector3.Angle(Direction, actualDirection);
            return !Mathf.Approximately(directionAngle, default);
        }

        private float CalcActualSquaredDistance() =>
            Vector3.SqrMagnitude(_targetPosition - Position);

        private Vector3 CalcActualDirection() =>
            Vector3.Normalize(_targetPosition - Position);
    }
}
