using UnityEngine;

namespace GameJam.Gameplay.Components.Speeding
{
    public class AcceleratedSpeedCalculator : SpeedCalculator
    {
        private readonly float _startSpeed;
        private readonly float _acceleration;

        private readonly float _minSpeed;
        private readonly float _maxSpeed;


        public AcceleratedSpeedCalculator(float startSpeed, float acceleration, float minSpeed, float maxSpeed)
        {
            _startSpeed = startSpeed;
            _acceleration = acceleration;
            _minSpeed = minSpeed;
            _maxSpeed = maxSpeed;
        }


        public override float GetCurrent(float timeSec) =>
            Mathf.Clamp(_startSpeed + _acceleration * timeSec, _minSpeed, _maxSpeed);
    }
}
