namespace GameJam.Gameplay.Components.Speeding
{
    public class ConstSpeedCalculator : SpeedCalculator
    {
        private readonly float _speed;

        public ConstSpeedCalculator(float speed) => _speed = speed;

        public override float GetCurrent(float timeSec) => _speed;
    }
}
