using UnityEngine;

namespace GameJam.Gameplay.Components.Speeding
{
    public class CurvedSpeedCalculator : SpeedCalculator
    {
        private readonly AnimationCurve _speedByTime;

        public CurvedSpeedCalculator(AnimationCurve speedByTime) => _speedByTime = speedByTime;

        public override float GetCurrent(float timeSec) => _speedByTime.Evaluate(timeSec);
    }
}
