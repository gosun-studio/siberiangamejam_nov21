namespace GameJam.Gameplay.Components.Speeding
{
    public abstract class SpeedCalculator
    {
        public abstract float GetCurrent(float timeSec);

    }
}
