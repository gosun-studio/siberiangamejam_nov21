﻿using System;
using UnityEngine;

namespace GameJam.Gameplay.Components.Speeding
{
    [Serializable]
    public class AcceleratedSpeedConfig : ValidatableClass
    {
        private const float MinSpeedLimit = float.NegativeInfinity;
        private const float MaxSpeedLimit = float.PositiveInfinity;

        [field: Header("Main params")]
        [field: SerializeField] public float StartSpeed { get; private set; }
        [field: SerializeField] public float Acceleration { get; private set; }

        [field: Header("Optional params")]
        [field: SerializeField] public float MinSpeed { get; private set; } = MinSpeedLimit;
        [field: SerializeField] public float MaxSpeed { get; private set; } = MaxSpeedLimit;
    }
}
