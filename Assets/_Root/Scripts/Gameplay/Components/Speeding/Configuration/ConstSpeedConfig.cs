﻿using System;
using UnityEngine;

namespace GameJam.Gameplay.Components.Speeding
{
    [Serializable]
    public class ConstSpeedConfig : ValidatableClass
    {
        [field: SerializeField] public float Speed { get; private set; }
    }
}
