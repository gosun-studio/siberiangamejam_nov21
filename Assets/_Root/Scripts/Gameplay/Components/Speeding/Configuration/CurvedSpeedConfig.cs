﻿using System;
using UnityEngine;

namespace GameJam.Gameplay.Components.Speeding
{
    [Serializable]
    public class CurvedSpeedConfig : ValidatableClass
    {
        [field: SerializeField] public AnimationCurve SpeedByTime { get; private set; }
    }
}
