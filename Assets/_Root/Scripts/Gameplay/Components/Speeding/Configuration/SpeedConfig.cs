﻿using System;
using UnityEngine;
using Sirenix.OdinInspector;

namespace GameJam.Gameplay.Components.Speeding
{
    [Serializable]
    public class SpeedConfig : ValidatableClass
    {
        [field: SerializeField] public SpeedType SpeedType { get; private set; } = SpeedType.Const;

        [field: SerializeField, HideLabel, ShowIf(nameof(SpeedType), SpeedType.Const)]
        public ConstSpeedConfig ConstSpeed { get; private set; } = null;

        [field: SerializeField, HideLabel, ShowIf(nameof(SpeedType), SpeedType.Curved)]
        public CurvedSpeedConfig CurvedSpeed { get; private set; } = null;

        [field: SerializeField, HideLabel, ShowIf(nameof(SpeedType), SpeedType.Accelerated)]
        public AcceleratedSpeedConfig AcceleratedSpeed { get; private set; } = null;
    }
}
