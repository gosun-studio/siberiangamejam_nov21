using System;

namespace GameJam.Gameplay.Components.Speeding
{
    public class SpeedCalculatorFactory
    {
        public SpeedCalculator Create(SpeedConfig speedConfig) =>
            speedConfig.SpeedType switch
            {
                SpeedType.Const => CreateForConst(speedConfig.ConstSpeed),
                SpeedType.Curved => CreateForCurved(speedConfig.CurvedSpeed),
                SpeedType.Accelerated => CreateForAccelerated(speedConfig.AcceleratedSpeed),
                _ => throw new ArgumentException($"Wrong {nameof(SpeedConfig.SpeedType)}")
            };

        private ConstSpeedCalculator CreateForConst(ConstSpeedConfig config) =>
            new ConstSpeedCalculator(config.Speed);

        private CurvedSpeedCalculator CreateForCurved(CurvedSpeedConfig config) =>
            new CurvedSpeedCalculator(config.SpeedByTime);

        private AcceleratedSpeedCalculator CreateForAccelerated(AcceleratedSpeedConfig config) =>
            new AcceleratedSpeedCalculator(config.StartSpeed, config.Acceleration, config.MinSpeed, config.MaxSpeed);
    }
}
