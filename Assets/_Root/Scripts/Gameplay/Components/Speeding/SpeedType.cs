﻿namespace GameJam.Gameplay.Components.Speeding
{
    public enum SpeedType
    {
        Const,
        Accelerated,
        Curved
    }
}
