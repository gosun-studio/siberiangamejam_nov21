﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace GameJam.Gameplay.Components.Timing
{
    public class StopwatchComponent : IDisposable
    {
        public event UnityAction Started;
        public event UnityAction Stopped;
        public event UnityAction Paused;
        public event UnityAction<float> Updated;

        public bool isPaused { get; private set; }
        public bool isCounting { get; private set; }
        public float CurrentTime { get; private set; }

        private bool _isDisposed;
        private Coroutine _routine;
        private MonoBehaviour _monoBehaviour;


        public StopwatchComponent(MonoBehaviour monoBehaviour) =>
            _monoBehaviour = monoBehaviour;

        public void Dispose()
        {
            if (_isDisposed)
                return;

            _isDisposed = true;
            StopCounting();
        }


        public void SetCurrentTime(float currentTime) =>
            CurrentTime = currentTime;

        public void StartCounting()
        {
            if (isPaused)
            {
                isPaused = false;
                return;
            }

            if (isCounting)
                return;

            StopCounting();

            isCounting = true;
            _routine = _monoBehaviour.StartCoroutine(CountingRoutine());

            Started?.Invoke();
        }

        public void PauseCounting()
        {
            if (!isCounting)
                return;

            if (isPaused)
                return;

            isPaused = true;
            Paused?.Invoke();
        }

        public void StopCounting()
        {
            if (!isCounting)
                return;

            isPaused = false;
            isCounting = false;
            CurrentTime = 0;

            if (_routine != null)
                _monoBehaviour.StopCoroutine(_routine);
            _routine = null;

            Stopped?.Invoke();
        }


        private IEnumerator CountingRoutine()
        {
            while (true)
            {
                if (!isPaused)
                {
                    float dt = Time.deltaTime;

                    Updated?.Invoke(dt);
                    CurrentTime += dt;
                }

                yield return null;
            }
            // ReSharper disable once IteratorNeverReturns
        }
    }
}
