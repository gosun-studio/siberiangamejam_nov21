﻿using System;
using UnityEngine.Events;

namespace GameJam.Gameplay.Components.Timing
{
    public class TimerComponent : IDisposable
    {
        public event UnityAction Started;
        public event UnityAction Updated;
        public event UnityAction Finished;

        private readonly StopwatchComponent _stopwatch;

        private bool _isDisposed;
        private int _lastStopwatchTime;

        public int SecondsPassed { get; private set; }
        public int SecondsCapacity { get; private set; }
        public int SecondsLeft => SecondsCapacity - SecondsPassed;
        public bool IsActive { get; private set; }
        public bool IsPaused { get; private set; }


        public TimerComponent(StopwatchComponent stopwatch)
        {
            _stopwatch = stopwatch;
            Subscribe();
        }

        public void Dispose()
        {
            if (_isDisposed)
                return;

            _isDisposed = true;
            StopTimer();
            Unsubscribe();

            _stopwatch.Dispose();
        }

        private void Subscribe() =>
            _stopwatch.Stopped += ResetStopwatchCache;

        private void Unsubscribe() =>
            _stopwatch.Stopped -= ResetStopwatchCache;


        public void SetCapacity(int secondsCapacity)
        {
            SecondsCapacity = secondsCapacity;
            Updated?.Invoke();
        }

        public void StartTimer(int secondsCapacity)
        {
            SetCapacity(secondsCapacity);
            StartTimer();
        }

        public void StartTimer()
        {
            if (IsActive)
                return;

            if (!IsPaused)
                ResetCount();

            IsActive = true;
            IsPaused = false;
            _stopwatch.Updated += OnStopwatchChanged;
            _stopwatch.StartCounting();

            Started?.Invoke();
            Updated?.Invoke();
        }

        public void StopTimer()
        {
            if (!IsActive)
                return;

            IsActive = false;
            IsPaused = false;
            _stopwatch.StopCounting();
            _stopwatch.Updated -= OnStopwatchChanged;
        }

        public void PauseTimer()
        {
            if (!IsActive)
                return;

            IsActive = false;
            IsPaused = true;
            _stopwatch.PauseCounting();
            _stopwatch.Updated -= OnStopwatchChanged;
        }

        private void ResetStopwatchCache() => _lastStopwatchTime = 0;
        private void ResetCount() => SetSecondsPassed(0);


        private void OnStopwatchChanged(float diff)
        {
            int currentStopwatchTime = (int)_stopwatch.CurrentTime;
            if (_lastStopwatchTime == currentStopwatchTime)
                return;

            int passedSeconds = currentStopwatchTime - _lastStopwatchTime;
            _lastStopwatchTime = currentStopwatchTime;

            SetSecondsPassed(SecondsPassed + passedSeconds);
            TryFinish();
        }

        private void SetSecondsPassed(int count)
        {
            SecondsPassed = count;
            Updated?.Invoke();
        }

        private void TryFinish()
        {
            if (SecondsLeft > 0)
                return;

            StopTimer();
            Finished?.Invoke();
        }
    }
}
