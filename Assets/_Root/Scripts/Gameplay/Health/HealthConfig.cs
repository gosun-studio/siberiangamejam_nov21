using UnityEngine;
using Sirenix.OdinInspector;

namespace GameJam.Gameplay.Health
{
    [CreateAssetMenu(fileName = nameof(HealthConfig), menuName = DefinitionCatalogs.Gameplay + nameof(HealthConfig))]
    public class HealthConfig : ValidatableScriptableObject
    {
        private const float Min = 0;
        private const float Max = 100;

        [field: SerializeField, MinValue(0)] public float MinHealth { get; private set; } = Min;
        [field: SerializeField] public float MaxHealth { get; private set; } = Max;
        [field: SerializeField] public float InitialHealth { get; private set; } = Max;
    }
}
