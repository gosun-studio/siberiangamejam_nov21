using UnityEngine.Events;

namespace GameJam.Gameplay.Health
{
    public class HealthController
    {
        public event UnityAction Changed
        {
            add => _model.Changed += value;
            remove => _model.Changed -= value;
        }
        public event UnityAction Filled
        {
            add => _model.Filled += value;
            remove => _model.Filled -= value;
        }
        public event UnityAction Wasted
        {
            add => _model.Wasted += value;
            remove => _model.Wasted -= value;
        }
        public event UnityAction Nullified
        {
            add => _model.Nullified += value;
            remove => _model.Nullified -= value;
        }
        public event UnityAction<float> Increased
        {
            add => _model.Increased += value;
            remove => _model.Increased -= value;
        }
        public event UnityAction<float> Decreased
        {
            add => _model.Decreased += value;
            remove => _model.Decreased -= value;
        }


        private readonly HealthView _view;
        private readonly HealthModel _model;

        private bool _isDispose;

        public float Current => _model.Current;
        public float Ratio => _model.Ratio;


        public HealthController(HealthView view, HealthModel model)
        {
            _view = view;
            _model = model;
            Subscribe();
        }

        public void Dispose()
        {
            if (_isDispose)
                return;

            _isDispose = true;
            Unsubscribe();
        }


        //private void Subscribe() => _model.Changed += OnChanged;
        private void Subscribe() => _model.Changed += HealthChanged;
        private void Unsubscribe() => _model.Changed -= OnChanged;
        private void OnChanged() => _view.SetSlider(_model.Ratio);


        public void Increase(float value) => _model.Increase(value);
        public void Decrease(float value) => _model.Decrease(value);

        public void Set(float value) => _model.Set(value);

        public void HealthChanged()
        {
            if (_model.Ratio == 0)
            {
                _view.ComicsPanel.sprite = _view.Comics0;
                _view.ComicsPanel.gameObject.SetActive(true);
            }
            if (_model.Ratio < 0)
            {
                _view.PrefabText.SetActive(true);
            }
            _view.SetSlider(_model.Ratio);
        }
    }
}
