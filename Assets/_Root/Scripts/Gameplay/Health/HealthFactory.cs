namespace GameJam.Gameplay.Health
{
    public class HealthFactory
    {
        public HealthController Create(HealthConfig config, HealthView view)
        {
            var model = new HealthModel(config.MinHealth, config.MaxHealth, config.InitialHealth);
            return new HealthController(view, model);
        }
    }
}
