using UnityEngine;
using UnityEngine.Events;

namespace GameJam.Gameplay.Health
{
    public class HealthModel
    {
        public event UnityAction Changed;
        public event UnityAction Filled;
        public event UnityAction Wasted;
        public event UnityAction Nullified;
        public event UnityAction<float> Increased;
        public event UnityAction<float> Decreased;

        private readonly float _max;
        private readonly float _min;

        public float Current { get; private set; }
        public float Ratio => _max > 0 ? Current / _max : 0;


        public HealthModel(float min, float max, float initial)
        {
            _min = min;
            _max = max;
            Current = initial;
        }


        public void Increase(float value) =>
            Set(Current + value);

        public void Decrease(float value) =>
            Set(Current - value);

        public void Set(float value)
        {
            if (Mathf.Approximately(Current, value))
                return;

            float diff = value - Current;
            Current = value;

            InvokeEvents(value, diff);
        }

        private void InvokeEvents(float actualValue, float difference)
        {
            Changed?.Invoke();

            if (difference > 0)
                Increased?.Invoke(difference);
            else
                Decreased?.Invoke(difference);

            if (Mathf.Approximately(actualValue, _min))
                Nullified?.Invoke();

            if (actualValue < _min)
                Wasted?.Invoke();

            else if (actualValue >= _max)
                Filled?.Invoke();
        }
    }
}
