using GameJam.Gameplay.Trap;

namespace GameJam.Gameplay.Health
{
    public class HealthTrapDirector
    {
        private readonly TrapsDirector _trapsDirector;
        private readonly HealthController _healthController;

        private bool _isDispose;


        public HealthTrapDirector(TrapsDirector trapsDirector, HealthController healthController)
        {
            _trapsDirector = trapsDirector;
            _healthController = healthController;
            Subscribe();
        }

        public void Dispose()
        {
            if (_isDispose)
                return;

            _isDispose = true;
            Unsubscribe();
        }

        private void Subscribe() => _trapsDirector.Damaged += OnDamaged;
        private void Unsubscribe() => _trapsDirector.Damaged -= OnDamaged;

        private void OnDamaged(float damage) =>
            _healthController.Decrease(damage);
    }
}
