using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;

namespace GameJam.Gameplay.Health
{
    public class HealthView : ValidatableMonoBehaviour
    {
        [ValidateInput(nameof(ValidateObject), NullError)]
        [SerializeField] private Slider _slider = null;

        public GameObject PrefabText;
        
        public Sprite Comics0;
        public Image ComicsPanel;

        private void OnValidate() =>
            _slider ??= GetComponent<Slider>();


        public void SetSlider(float ratio) => _slider.value = ratio;
    }
}
