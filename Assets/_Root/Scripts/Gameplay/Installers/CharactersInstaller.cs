using UnityEngine;
using Sirenix.OdinInspector;
using GameJam.Gameplay.Character;
using GameJam.Gameplay.Character.Cat;
using GameJam.Gameplay.Character.Player;
using GameJam.Gameplay.Components.Speeding;

namespace GameJam.Gameplay.Installers
{
    public class CharactersInstaller : ValidatableMonoInstaller
    {
        [ValidateInput(nameof(ValidateObject), NullError)]
        [SerializeField] private CatView _catView = null;

        [ValidateInput(nameof(ValidateObject), NullError)]
        [SerializeField] private PlayerView _playerView = null;


        public override void InstallBindings()
        {
            CharacterFactory factory = CreateFactory();
            BindCat(factory);
            BindPlayer(factory);
        }

        private CharacterFactory CreateFactory()
        {
            var speedCalculatorFactory = new SpeedCalculatorFactory();
            return new CharacterFactory(speedCalculatorFactory);
        }

        private void BindCat(CharacterFactory factory)
        {
            var catConfig = _catView.GetComponent<CatConfig>();
            CatController catController = factory.Create(catConfig, _catView);
            Container.Bind<CatController>().FromInstance(catController).AsCached();

            Container.Bind<CatClickingDirector>().AsCached();
        }

        private void BindPlayer(CharacterFactory factory)
        {
            var playerConfig = _playerView.GetComponent<PlayerConfig>();
            PlayerController playerController = factory.Create(playerConfig, _playerView);
            Container.Bind<PlayerController>().FromInstance(playerController).AsCached();

            Container.Bind<PlayerScenarioDirector>().AsCached();
        }
    }
}
