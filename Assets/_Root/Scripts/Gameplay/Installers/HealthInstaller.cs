using UnityEngine;
using Sirenix.OdinInspector;
using GameJam.Gameplay.Health;
using UnityEngine.UI;

namespace GameJam.Gameplay.Installers
{
    public class HealthInstaller : ValidatableMonoInstaller
    {
        [ValidateInput(nameof(ValidateObject), NullError)]
        [SerializeField] private HealthConfig _healthConfig = null;

        [ValidateInput(nameof(ValidateObject), NullError)]
        [SerializeField] private HealthView _healthView = null;


        public override void InstallBindings()
        {
            var factory = new HealthFactory();
            HealthController controller = factory.Create(_healthConfig, _healthView);

            Container.Bind<HealthController>().FromInstance(controller).AsCached();
            Container.Bind<HealthTrapDirector>().AsCached();
        }
        
        
    }
}
