using UnityEngine;
using Sirenix.OdinInspector;
using GameJam.Systems.PlayerInput;
using GameJam.Gameplay.Components.Clicking;

namespace GameJam.Gameplay.Installers
{
    public class InputInstaller : ValidatableMonoInstaller
    {
        [ValidateInput(nameof(ValidateObject), NullError)]
        [SerializeField] private InputNotifier _inputNotifier = null;

        [ValidateInput(nameof(ValidateObject), NullError)]
        [SerializeField] private TrapClickDetector trapClickDetector = null; 

        [ValidateInput(nameof(ValidateObject), NullError)]
        [SerializeField] private MovingPlaneClickDetector movingPlaneClickDetector = null;


        public override void InstallBindings()
        {
            Container.Bind<InputNotifier>().FromInstance(_inputNotifier).AsCached();
            Container.Bind<TrapClickDetector>().FromInstance(trapClickDetector).AsCached();
            Container.Bind<MovingPlaneClickDetector>().FromInstance(movingPlaneClickDetector).AsCached();
        }
    }
}
