using UnityEngine;
using GameJam.Gameplay.Level;
using Sirenix.OdinInspector;
using UnityEngine.UI;

namespace GameJam.Gameplay.Installers
{
    public class LevelInstaller : ValidatableMonoInstaller
    {
        [ValidateInput(nameof(ValidateObject), NullError)]
        [SerializeField] private GameObject _levelCompleteView = null;
        [SerializeField] private Sprite Comics;
        [SerializeField] private Image ComicsPanel;


        public override void InstallBindings()
        {
            Container.Bind<LevelController>().AsSingle().NonLazy();
            Container.Bind<LevelFailedListener>().AsCached().NonLazy();
            Container.Bind<LevelSucceedListener>().AsCached().NonLazy();
            Container.Bind<Sprite>().FromInstance(Comics)
                .AsCached().WhenInjectedInto<LevelEndDirector>();
            Container.Bind<Image>().FromInstance(ComicsPanel)
                .AsCached().WhenInjectedInto<LevelEndDirector>();
            Container.Bind<GameObject>().FromInstance(_levelCompleteView)
                .AsCached().WhenInjectedInto<LevelEndDirector>();
            Container.Bind<LevelEndDirector>().AsCached().NonLazy();
        }
    }
}
