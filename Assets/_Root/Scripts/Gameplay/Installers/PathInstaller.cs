using UnityEngine;
using Sirenix.OdinInspector;
using GameJam.Gameplay.Paths;

namespace GameJam.Gameplay.Installers
{
    public class PathInstaller : ValidatableMonoInstaller
    {
        [ValidateInput(nameof(ValidateObject), NullError)]
        [SerializeField] private PathRoster _roster;


        private void OnValidate() => _roster ??= GetComponent<PathRoster>();

        public override void InstallBindings()
        {
            Container.Bind<PathsRepository>().FromMethod(CreatePathsRepository);
            Container.Bind<PathProgressFactory>().AsCached();
            Container.Bind<PathsDirector>().AsCached();
        }

        private PathsRepository CreatePathsRepository()
        {
            var factory = new PathFactory();
            var initializer = new PathsInitializer(_roster, factory);
            PathController[] pathControllers = initializer.InitializePaths();

            return new PathsRepository(pathControllers);
        }
    }
}
