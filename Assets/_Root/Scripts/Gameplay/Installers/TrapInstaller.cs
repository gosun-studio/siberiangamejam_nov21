using UnityEngine;
using Sirenix.OdinInspector;
using GameJam.Gameplay.Trap;
using GameJam.Gameplay.Components.Clicking;

namespace GameJam.Gameplay.Installers
{
    public class TrapInstaller : ValidatableMonoInstaller
    {
        [ValidateInput(nameof(ValidateObject), NullError)]
        [SerializeField] private TrapRoster _roster;


        private void OnValidate() => _roster ??= GetComponent<TrapRoster>();

        public override void InstallBindings()
        {
            Container.Bind<TrapsRepository>().FromMethod(CreateTrapsRepository);
            Container.Bind<TrapClickingDirector>().AsCached();
            Container.Bind<TrapsDirector>().AsCached();
        }

        private TrapsRepository CreateTrapsRepository()
        {
            var factory = new TrapFactory();
            var initializer = new TrapsInitializer(_roster, factory);
            TrapController[] trapControllers = initializer.InitializeTraps();

            return new TrapsRepository(trapControllers);
        }
    }
}
