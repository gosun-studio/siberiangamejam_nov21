using GameJam.Gameplay.Trap;
using GameJam.Gameplay.Character.Cat;
using GameJam.Gameplay.Character.Player;
using GameJam.Gameplay.Health;

namespace GameJam.Gameplay.Level
{
    public class LevelController
    {
        private readonly HealthTrapDirector _healthTrapDirector;
        private readonly CatClickingDirector _catClickingDirector;
        private readonly TrapClickingDirector _trapClickingDirector;
        private readonly PlayerScenarioDirector _playerScenarioDirector;

        public LevelController(
            HealthTrapDirector healthTrapDirector,
            CatClickingDirector catClickingDirector, TrapClickingDirector trapClickingDirector,
            PlayerScenarioDirector playerScenarioDirector)
        {
            _healthTrapDirector = healthTrapDirector;
            _catClickingDirector = catClickingDirector;
            _trapClickingDirector = trapClickingDirector;
            _playerScenarioDirector = playerScenarioDirector;

            _playerScenarioDirector.Start();
        }
    }
}
