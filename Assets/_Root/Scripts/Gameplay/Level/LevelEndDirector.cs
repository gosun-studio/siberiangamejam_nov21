using System;
using UnityEngine;
using UnityEngine.UI;

namespace GameJam.Gameplay.Level
{
    public class LevelEndDirector : IDisposable
    {
        private readonly LevelSucceedListener _succeedListener;
        private readonly LevelFailedListener _failedListener;
        private readonly GameObject _view;
        private readonly Sprite Comics;
        private readonly Image ComicsPanel;

        private bool _isDispose;


        public LevelEndDirector(LevelSucceedListener succeedListener, LevelFailedListener failedListener, GameObject view, Sprite comics, Image comicsPanel)
        {
            _succeedListener = succeedListener;
            _failedListener = failedListener;
            _view = view;
            Comics = comics;
            ComicsPanel = comicsPanel;
            Subscribe();
            Time.timeScale = 1; // >.<
        }

        public void Dispose()
        {
            if (_isDispose)
                return;

            _isDispose = true;
            Unsubscribe();
        }


        private void Subscribe()
        {
            _succeedListener.Succeed += OnSucceed;
            _failedListener.Failed += OnFailed;
        }

        private void Unsubscribe()
        {
            _succeedListener.Succeed -= OnSucceed;
            _failedListener.Failed -= OnFailed;
        }

        private void OnFailed() => OnCompleted();
        private void OnSucceed() => OnCompleted();

        private void OnCompleted()
        {
            // госпади прости за всё это
            Time.timeScale = 0;
            ComicsPanel.sprite = Comics;
            ComicsPanel.gameObject.SetActive(true);
            _view.SetActive(true);
        }
    }
}
