using System;
using GameJam.Gameplay.Character.Player;
using GameJam.Gameplay.Trap;
using GameJam.Gameplay.Health;
using UnityEngine.Events;

namespace GameJam.Gameplay.Level
{
    public class LevelFailedListener : IDisposable
    {
        public event UnityAction Failed;

        private readonly TrapsDirector _trapsDirector;
        private readonly HealthController _healthController;
        private readonly PlayerScenarioDirector _scenarioDirector;

        private bool _isDispose;


        public LevelFailedListener(TrapsDirector trapsDirector, HealthController healthController, PlayerScenarioDirector scenarioDirector)
        {
            _trapsDirector = trapsDirector;
            _healthController = healthController;
            _scenarioDirector = scenarioDirector;
            Subscribe();
        }

        public void Dispose()
        {
            if (_isDispose)
                return;

            _isDispose = true;
            Unsubscribe();
        }

        private void Subscribe()
        {
            _trapsDirector.Critical += OnCritical;
            _healthController.Wasted += OnWasted;
            _scenarioDirector.PathCompleted += OnPathCompleted;
        }

        private void Unsubscribe()
        {
            _trapsDirector.Critical -= OnCritical;
            _healthController.Wasted -= OnWasted;
            _scenarioDirector.PathCompleted -= OnPathCompleted;
        }


        private void OnCritical() => Failed?.Invoke();
        private void OnWasted() => Failed?.Invoke();
        private void OnPathCompleted() => Failed?.Invoke();
    }
}
