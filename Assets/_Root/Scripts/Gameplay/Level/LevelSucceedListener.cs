using UnityEngine.Events;
using GameJam.Gameplay.Health;

namespace GameJam.Gameplay.Level
{
    public class LevelSucceedListener
    {
        public event UnityAction Succeed;

        private readonly HealthController _healthController;

        private bool _isDispose;


        public LevelSucceedListener(HealthController healthController)
        {
            _healthController = healthController;
            Subscribe();
        }

        public void Dispose()
        {
            if (_isDispose)
                return;

            _isDispose = true;
            Unsubscribe();
        }

        private void Subscribe() => _healthController.Nullified += OnNullified;
        private void Unsubscribe() => _healthController.Nullified -= OnNullified;


        private void OnNullified() => Succeed?.Invoke();
    }
}
