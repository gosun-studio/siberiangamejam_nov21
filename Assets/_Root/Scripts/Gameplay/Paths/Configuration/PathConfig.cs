using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace GameJam.Gameplay.Paths
{
    public class PathConfig : ValidatableMonoBehaviour
    {
        [field: SerializeField] public bool Looped { get; private set; } = false;

        [SerializeField] private List<PathPointConfig> _orderedPoints = new List<PathPointConfig>();

        public IReadOnlyList<PathPointConfig> Points => _orderedPoints;


        private void OnValidate()
        {
            RemoveNullPoints();
            UpdateList();
        }

        private void Start() => OnValidate();


        [Button(ButtonSizes.Medium)]
        private void UpdateList() =>
            _orderedPoints = GetComponentsInChildren<PathPointConfig>().ToList();

        private void RemoveNullPoints()
        {
            const int startIndex = 0;
            int lastIndex = _orderedPoints.Count - 1;

            if (lastIndex < startIndex)
                return;

            for (int i = lastIndex; i >= startIndex; i--)
                if (_orderedPoints[i] == null)
                    _orderedPoints.RemoveAt(i);
        }
    }
}
