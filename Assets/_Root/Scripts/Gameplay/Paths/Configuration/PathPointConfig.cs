using UnityEngine;

namespace GameJam.Gameplay.Paths
{
    public class PathPointConfig : ValidatableMonoBehaviour
    {
        private Transform _transformCache;
        private Transform Transform => _transformCache ??= transform;

        public Vector3 Position => Transform.position;
    }
}
