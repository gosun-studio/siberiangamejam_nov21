using UnityEngine;

namespace GameJam.Gameplay.Paths
{
    public class PathsDirector
    {
        private readonly PathsRepository _repository;
        private readonly PathProgressFactory _factory;


        public PathsDirector(PathsRepository repository, PathProgressFactory factory)
        {
            _repository = repository;
            _factory = factory;
        }


        public PathProgress GetPathProgress(Vector3 startPoint)
        {
            PathController path = _repository.GetNearestPath(startPoint);
            return _factory.CreateDirect(startPoint, path);
        }

        public PathProgress GetPathProgressReversed(Vector3 startPoint)
        {
            PathController path = _repository.GetNearestPath(startPoint);
            return _factory.CreateReversed(startPoint, path);
        }
    }
}
