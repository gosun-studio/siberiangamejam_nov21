using System.Linq;
using UnityEngine;
using System.Collections.Generic;

namespace GameJam.Gameplay.Paths
{
    public class PathsRepository
    {
        private readonly PathController[] _paths;

        public bool IsEmpty => _paths.LengthSafe() == 0;
        public IReadOnlyCollection<PathController> Paths => _paths;


        public PathsRepository(IReadOnlyCollection<PathController> paths) =>
            _paths = paths.ToArray();


        public PathController GetRandomPath()
        {
            const int minIndex = 0;
            int maxIndex = _paths.Length;
            int randomIndex = Random.Range(minIndex, maxIndex);

            return _paths[randomIndex];
        }

        public PathController GetNearestPath(Vector3 position)
        {
            PathController nearestPath = null;
            float minSquaredDistance = float.PositiveInfinity;

            foreach (PathController path in _paths)
            {
                Vector3 nearestPoint = path.GetNearestPoint(position);
                float squaredDistance = Vector3.SqrMagnitude(nearestPoint - position);

                if (squaredDistance < minSquaredDistance)
                {
                    minSquaredDistance = squaredDistance;
                    nearestPath = path;
                }
            }

            return nearestPath;
        }
    }
}
