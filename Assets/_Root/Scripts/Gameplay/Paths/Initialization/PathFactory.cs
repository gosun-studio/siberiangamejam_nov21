using UnityEngine;
using System.Linq;
using System.Collections.Generic;

namespace GameJam.Gameplay.Paths
{
    public class PathFactory
    {
        public PathController Create(PathConfig config)
        {
            PathModel model = CreateModel(config);
            return new PathController(model);
        }

        private PathModel CreateModel(PathConfig config)
        {
            IReadOnlyList<PathPointConfig> points = config.Points;
            Vector3[] vectorPoints = points.Select(p => p.Position).ToArray();

            return config.Looped switch
            {
                true => new LoopedPathModel(vectorPoints),
                false => new FinitePathModel(vectorPoints)
            };
        }
    }
}
