using System;
using UnityEngine;
using System.Collections.Generic;
using Sirenix.OdinInspector;

namespace GameJam.Gameplay.Paths
{
    public class PathRoster : ValidatableMonoBehaviour
    {
        [GUIColor(nameof(FieldColor))]
        [SerializeField, ReadOnly] private PathConfig[] _paths = Array.Empty<PathConfig>();

        private bool HasData => !_paths.IsNullOrEmpty();
        public IReadOnlyCollection<PathConfig> Paths => _paths;


        private void OnValidate()
        {
            FillData();
            ValidateField(HasData);
        }

        private void Start() => OnValidate();


        [Button(ButtonSizes.Medium)]
        private void FillData() => _paths = FindObjectsOfType<PathConfig>();
    }
}
