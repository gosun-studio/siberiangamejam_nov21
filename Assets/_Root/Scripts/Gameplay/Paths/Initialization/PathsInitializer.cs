using System.Linq;

namespace GameJam.Gameplay.Paths
{
    public class PathsInitializer
    {
        private readonly PathRoster _roster;
        private readonly PathFactory _factory;


        public PathsInitializer(PathRoster roster, PathFactory factory)
        {
            _roster = roster;
            _factory = factory;
        }


        public PathController[] InitializePaths() =>
            _roster.Paths
                .Select(pathConfig => _factory.Create(pathConfig))
                .ToArray();
    }
}
