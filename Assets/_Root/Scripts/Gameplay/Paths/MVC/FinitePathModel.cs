using UnityEngine;
using System.Collections.Generic;

namespace GameJam.Gameplay.Paths
{
    public class FinitePathModel : PathModel
    {
        public override bool Looped => false;


        public FinitePathModel(IReadOnlyCollection<Vector3> points) : base(points)
        { }


        public override bool HasNextIndex(int index) => ContainsIndex(GetNextIndex(index));
        public override bool HasPrevIndex(int index) => ContainsIndex(GetPrevIndex(index));

        public override int GetNextIndex(int index) => index + 1;
        public override int GetPrevIndex(int index) => index - 1;
    }
}
