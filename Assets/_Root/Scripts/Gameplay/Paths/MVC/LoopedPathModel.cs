using System.Collections.Generic;
using UnityEngine;

namespace GameJam.Gameplay.Paths
{
    public class LoopedPathModel : PathModel
    {
        public override bool Looped => true;


        public LoopedPathModel(IReadOnlyCollection<Vector3> points) : base(points)
        { }


        public override bool HasNextIndex(int index) => ContainsIndex(index);
        public override bool HasPrevIndex(int index) => ContainsIndex(index);

        public override int GetPrevIndex(int index)
        {
            int prevIndex = index - 1;
            while (prevIndex < _minIndex)
                prevIndex += _points.Length;

            return prevIndex;
        }

        public override int GetNextIndex(int index)
        {
            int nextIndex = index + 1;
            while (nextIndex > _maxIndex)
                nextIndex -= _points.Length;

            return nextIndex;
        }
    }
}
