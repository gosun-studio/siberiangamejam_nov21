using UnityEngine;
using System.Collections.Generic;

namespace GameJam.Gameplay.Paths
{
    public class PathController
    {
        private readonly PathModel _model;

        public bool IsEmpty => _model.IsEmpty;
        public IReadOnlyList<Vector3> Points => _model.Points;


        public PathController(PathModel model) =>
            _model = model;


        public bool HasNextPoint(int index) => _model.HasNextIndex(index);
        public bool HasPrevPoint(int index) => _model.HasPrevIndex(index);

        public int GetNextIndex(int index) => _model.GetNextIndex(index);
        public int GetPrevIndex(int index) => _model.GetPrevIndex(index);

        public Vector3 GetNextPoint(int index) => _model.Points[GetNextIndex(index)];
        public Vector3 GetPrevPoint(int index) => _model.Points[GetPrevIndex(index)];

        public Vector3 GetNearestPoint(Vector3 position)
        {
            int nearestPointIndex = GetNearestPointIndex(position);
            return _model.Points[nearestPointIndex];
        }

        public int GetNearestPointIndex(Vector3 position)
        {
            int nearestPointIndex = 0;
            float minSquaredDistance = float.PositiveInfinity;

            for (int i = 0; i < Points.Count; i++)
            {
                Vector3 point = Points[i];
                float squaredDistance = Vector3.SqrMagnitude(position - point);

                if (squaredDistance < minSquaredDistance)
                {
                    nearestPointIndex = i;
                    minSquaredDistance = squaredDistance;
                }
            }

            return nearestPointIndex;
        }
    }
}
