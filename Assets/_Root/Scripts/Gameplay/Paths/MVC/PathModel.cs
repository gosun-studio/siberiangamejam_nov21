using UnityEngine;
using System.Linq;
using System.Collections.Generic;

namespace GameJam.Gameplay.Paths
{
    public abstract class PathModel
    {
        protected readonly int _minIndex;
        protected readonly int _maxIndex;
        protected readonly Vector3[] _points;

        public abstract bool Looped { get; }
        public IReadOnlyList<Vector3> Points => _points;
        public bool IsEmpty => _points.LengthSafe() == 0;


        protected PathModel(IReadOnlyCollection<Vector3> points)
        {
            _points = points.ToArray();
            _minIndex = 0;
            _maxIndex = _points.Length - 1;
        }


        public abstract bool HasNextIndex(int index);
        public abstract bool HasPrevIndex(int index);

        public abstract int GetPrevIndex(int index);
        public abstract int GetNextIndex(int index);


        protected bool ContainsIndex(int index) =>
            _minIndex <= index && index <= _maxIndex;
    }
}
