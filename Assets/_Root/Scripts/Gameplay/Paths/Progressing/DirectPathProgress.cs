using UnityEngine;

namespace GameJam.Gameplay.Paths
{
    public class DirectPathProgress : PathProgress
    {
        public DirectPathProgress(Vector3 startPosition, PathController path) : base(startPosition, path)
        { }

        protected override bool HasNextPoint(int currentIndex) =>
            _path.HasNextPoint(currentIndex);

        protected override int GetNextIndex(int currentIndex) =>
            _path.GetNextIndex(currentIndex);
    }
}
