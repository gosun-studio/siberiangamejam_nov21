using UnityEngine;
using UnityEngine.Events;

namespace GameJam.Gameplay.Paths
{
    public abstract class PathProgress
    {
        public event UnityAction PathCompleted;
        public event UnityAction PointCompleted;

        protected readonly PathController _path;

        public bool IsCompleted { get; private set; }
        public int CurrentIndex { get; private set; }
        public Vector3 CurrentPoint => _path.Points[CurrentIndex];


        protected PathProgress(Vector3 startPosition, PathController path)
        {
            _path = path;
            CurrentIndex = _path.GetNearestPointIndex(startPosition);
            IsCompleted = CalcIsCompleted();
        }


        public void CompletePoint()
        {
            if (HasNextPoint(CurrentIndex))
            {
                CurrentIndex = GetNextIndex(CurrentIndex);
                PointCompleted?.Invoke();
            }
            else
            {
                IsCompleted = true;
                PathCompleted?.Invoke();
            }
        }

        protected abstract bool HasNextPoint(int currentIndex);
        protected abstract int GetNextIndex(int currentIndex);

        private bool CalcIsCompleted() => !HasNextPoint(CurrentIndex);
    }
}
