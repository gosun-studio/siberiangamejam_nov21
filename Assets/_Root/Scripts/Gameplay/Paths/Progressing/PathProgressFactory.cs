using UnityEngine;

namespace GameJam.Gameplay.Paths
{
    public class PathProgressFactory
    {
        public PathProgress CreateDirect(Vector3 startPosition, PathController path) =>
            new DirectPathProgress(startPosition, path);

        public PathProgress CreateReversed(Vector3 startPosition, PathController path) =>
            new ReversedPathProgress(startPosition, path);
    }
}
