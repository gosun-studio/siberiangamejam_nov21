using UnityEngine;

namespace GameJam.Gameplay.Paths
{
    public class ReversedPathProgress : PathProgress
    {
        public ReversedPathProgress(Vector3 startPosition, PathController path) : base(startPosition, path)
        { }

        protected override bool HasNextPoint(int currentIndex) =>
            _path.HasPrevPoint(currentIndex);

        protected override int GetNextIndex(int currentIndex) =>
            _path.GetPrevIndex(currentIndex);
    }
}
