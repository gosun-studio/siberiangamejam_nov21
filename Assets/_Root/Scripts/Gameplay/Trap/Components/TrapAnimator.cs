using System.ComponentModel;
using Sirenix.OdinInspector;
using UnityEngine;

namespace GameJam.Gameplay.Trap.Components
{
    public class TrapAnimator : ValidatableMonoBehaviour
    {
        [Header("Components")]
        [ValidateInput(nameof(ValidateObject), NullError)]
        [SerializeField] private Animator _animator = null;

        [Header("Triggers")]
        [SerializeField] private string LockedTrigger = "";
        [SerializeField] private string IdleTrigger = "";
        [SerializeField] private string ReadyTrigger = "";
        [SerializeField] private string ActiveTrigger = "";
        [SerializeField] private string CompletedTrigger = "";


        private void OnValidate() =>
            _animator ??= GetComponent<Animator>();


        public void PlayState(TrapState state)
        {
            string triggerName = GetTrigger(state);
            _animator.SetTrigger(triggerName);
        }

        private string GetTrigger(TrapState state) =>
            state switch
            {
                TrapState.Locked => LockedTrigger,
                TrapState.Idle => IdleTrigger,
                TrapState.Ready => ReadyTrigger,
                TrapState.Active => ActiveTrigger,
                TrapState.Completed => CompletedTrigger,
                _ => throw new InvalidEnumArgumentException()
            };
    }
}
