using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.Rendering;
using UnityEngine.UI;

namespace GameJam.Gameplay.Trap
{
    public class TrapConfig : ValidatableMonoBehaviour
    {
        [ValidateInput(nameof(ValidateId))]
        [SerializeField] private string _id = "";

        [Tooltip("Критическая ловушка форсит поражение на уровне")]
        [SerializeField] private bool _isCritical = false;

        public Sprite Comics;
        public Image ComicsPanel;
        
        [HideIf(nameof(_isCritical)), MinValue(0)]
        [Tooltip("Урон, наносимый игроку при активации ловушки")]
        [SerializeField] private float _damage = 0;

        [HideIf(nameof(_isCritical))]
        [ValidateInput(nameof(ValidateDuplicates), DuplicatesError)]
        [ValidateInput(nameof(ValidateCriticalTrapHasNoUnlockingTraps))]
        [ValidateInput(nameof(ValidateArrayExcludeThis), "Can't contain itself!")]
        [Tooltip("Список ловушек, которые будут разблокированы после активации текущей")]
        [SerializeField] private TrapConfig[] _unlockingTraps = Array.Empty<TrapConfig>();

        public string Id => _id;
        public float Damage => _damage;
        public bool IsCritical => _isCritical;
        public IEnumerable<string> UnlockingTraps => _unlockingTraps.Select(t => t.Id);


        private bool ValidateId() => !_id.IsNullOrEmpty();
        private bool ValidateArrayExcludeThis(TrapConfig[] trapConfigs) => !trapConfigs.Contains(this);
        private bool ValidateCriticalTrapHasNoUnlockingTraps()
        {
            if (_isCritical && !_unlockingTraps.IsNullOrEmpty())
                _unlockingTraps = Array.Empty<TrapConfig>();

            return true;
        }
    }
}
