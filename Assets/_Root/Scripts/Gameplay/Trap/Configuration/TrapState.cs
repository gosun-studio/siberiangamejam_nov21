namespace GameJam.Gameplay.Trap
{
    public enum TrapState
    {
        Locked,     // ловушка заблокирована другой ловушкой
        Idle,       // ловушка ожидает прихода кота
        Ready,      // ловушка может быть активирована котом
        Active,     // ловушка активирована и ожидает игрока
        Completed   // ловушка сработала
    }
}
