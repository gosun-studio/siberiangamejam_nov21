using UnityEngine.Events;

namespace GameJam.Gameplay.Trap.Repository
{
    public class RepositoryData
    {
        public readonly TrapController Controller;
        public readonly UnityAction ActivatedAction;
        public readonly UnityAction CompletedAction;


        public RepositoryData(TrapController controller, UnityAction activatedAction, UnityAction completedAction)
        {
            Controller = controller;
            ActivatedAction = activatedAction;
            CompletedAction = completedAction;
        }


        public void Subscribe()
        {
            Controller.Activated += ActivatedAction;
            Controller.Completed += CompletedAction;
        }

        public void Unsubscribe()
        {
            Controller.Activated -= ActivatedAction;
            Controller.Completed -= CompletedAction;
        }
    }
}
