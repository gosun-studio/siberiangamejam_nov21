using System;
using GameJam.Gameplay.Components.Clicking;

namespace GameJam.Gameplay.Trap
{
    public class TrapClickingDirector : IDisposable
    {
        private readonly TrapClickDetector _detector;

        private bool _isDisposed;


        public TrapClickingDirector(TrapClickDetector detector)
        {
            _detector = detector;
            Subscribe();
        }

        public void Dispose()
        {
            if (_isDisposed)
                return;

            _isDisposed = true;
            Unsubscribe();
        }


        private void Subscribe() => _detector.ClickDetected += OnClickDetected;
        private void Unsubscribe() => _detector.ClickDetected -= OnClickDetected;

        private void OnClickDetected(TrapView trapView) => trapView.Click();
    }
}
