using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace GameJam.Gameplay.Trap
{
    public class TrapsDirector : IDisposable
    {
        public event Action Critical;
        public event Action<float> Damaged;

        private readonly TrapsRepository _repository;

        private bool _isDisposed;


        public TrapsDirector(TrapsRepository repository)
        {
            _repository = repository;
            Subscribe();
            LockInitialTraps();
        }

        public void Dispose()
        {
            if (_isDisposed)
                return;

            _isDisposed = true;
            Unsubscribe();
        }


        private void LockInitialTraps()
        {
            foreach (TrapController trapController in _repository.Traps)
            foreach (string trapId in trapController.UnlockingTraps)
                _repository[trapId].Lock();
        }

        private void Subscribe()
        {
            _repository.Activated += OnActivated;
            _repository.Completed += OnCompleted;
        }

        private void Unsubscribe()
        {
            _repository.Activated -= OnActivated;
            _repository.Completed -= OnCompleted;
        }


        private void OnActivated(TrapController trapController)
        {
            foreach (string trapId in trapController.UnlockingTraps)
                _repository[trapId].Unlock();
        }

        private void OnCompleted(TrapController trapController)
        {
            float damage = trapController.Damage;
            bool isCritical = trapController.IsCritical;

            if (isCritical)
            {
                trapController.ComicsPanel.sprite = trapController.Comics;
                trapController.ComicsPanel.gameObject.SetActive(true);
                Critical?.Invoke();
            }
            else
                Damaged?.Invoke(damage);
        }
    }
}
