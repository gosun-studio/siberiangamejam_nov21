using System.Linq;
using System.Collections.Generic;
using GameJam.Gameplay.Trap.Repository;
using UnityEngine.Events;

namespace GameJam.Gameplay.Trap
{
    public class TrapsRepository
    {
        public event UnityAction<TrapController> Activated;
        public event UnityAction<TrapController> Completed;

        private readonly Dictionary<string, RepositoryData> _data;

        private bool _isDisposed;

        public TrapController this[string trapId] => _data[trapId].Controller;
        public IEnumerable<TrapController> Traps => _data.Values.Select(d => d.Controller);


        public TrapsRepository(TrapController[] trapControllers)
        {
            _data = trapControllers.ToDictionary
            (
                tc => tc.Id,
                tc => new RepositoryData
                (
                    tc,
                    () => Activated?.Invoke(tc),
                    () => Completed?.Invoke(tc)
                )
            );

            Subscribe();
        }

        public void Dispose()
        {
            if (_isDisposed)
                return;

            _isDisposed = true;
            Unsubscribe();

            _data.Clear();
        }


        private void Subscribe()
        {
            foreach (RepositoryData repositoryData in _data.Values)
                repositoryData.Subscribe();
        }

        private void Unsubscribe()
        {
            foreach (RepositoryData repositoryData in _data.Values)
                repositoryData.Unsubscribe();
        }
    }
}
