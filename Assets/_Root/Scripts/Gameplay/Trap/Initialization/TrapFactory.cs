namespace GameJam.Gameplay.Trap
{
    public class TrapFactory
    {
        public TrapController Create(TrapConfig config, TrapView view)
        {
            var model = new TrapModel(config);
            var controller = new TrapController(view, model);

            return controller;
        }
    }
}
