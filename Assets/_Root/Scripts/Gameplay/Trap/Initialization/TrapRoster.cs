using System;
using System.Linq;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace GameJam.Gameplay.Trap
{
    public class TrapRoster : ValidatableMonoBehaviour
    {
        [GUIColor(nameof(FieldColor))]
        [SerializeField, ReadOnly] private TrapRosterData[] _datas = Array.Empty<TrapRosterData>();

        private bool HasData => !_datas.IsNullOrEmpty();
        public IReadOnlyCollection<TrapRosterData> Datas => _datas;


        private void Start() => OnValidate();

        private void OnValidate()
        {
            FillData();
            ValidateField(HasData);
        }

        [Button(ButtonSizes.Medium)]
        private void FillData()
        {
            _datas = FindObjectsOfType<TrapView>()
                .Select(CreateData)
                .ToArray();
        }

        private TrapRosterData CreateData(TrapView view) =>
            new TrapRosterData(view.GetComponent<TrapConfig>(), view);
    }
}
