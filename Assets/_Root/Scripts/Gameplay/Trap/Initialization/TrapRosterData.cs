using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace GameJam.Gameplay.Trap
{
    [Serializable]
    public class TrapRosterData : ValidatableClass
    {
        [field: ValidateInput(nameof(ValidateObject), NullError)]
        [field: SerializeField] public TrapConfig Config { get; private set; }

        [field: ValidateInput(nameof(ValidateObject), NullError)]
        [field: SerializeField] public TrapView View { get; private set; }


        public TrapRosterData(TrapConfig config, TrapView view)
        {
            Config = config;
            View = view;
        }
    }
}
