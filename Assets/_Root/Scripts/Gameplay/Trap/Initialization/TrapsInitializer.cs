using System.Linq;

namespace GameJam.Gameplay.Trap
{
    public class TrapsInitializer
    {
        private readonly TrapRoster _roster;
        private readonly TrapFactory _factory;


        public TrapsInitializer(TrapRoster roster, TrapFactory factory)
        {
            _roster = roster;
            _factory = factory;
        }


        public TrapController[] InitializeTraps() =>
            _roster.Datas
                .Select(data => _factory.Create(data.Config, data.View))
                .ToArray();
    }
}
