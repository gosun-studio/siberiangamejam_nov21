using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Object = UnityEngine.Object;

namespace GameJam.Gameplay.Trap
{
    public class TrapController : IDisposable
    {
        public event UnityAction Activated;
        public event UnityAction Completed;

        private readonly TrapView _view;
        private readonly TrapModel _model;

        private bool _isDisposed;

        public Sprite Comics => _model.Comics;
        public Image ComicsPanel => _model.ComicsPanel;
        public string Id => _model.Id;
        public float Damage => _model.Damage;
        public bool IsCritical => _model.IsCritical;
        public IReadOnlyCollection<string> UnlockingTraps => _model.UnlockingTraps;


        public TrapController(TrapView view, TrapModel model)
        {
            _view = view;
            _model = model;
            Subscribe();
        }

        public void Dispose()
        {
            if (_isDisposed)
                return;

            _isDisposed = true;
            Unsubscribe();

            Object.Destroy(_view.gameObject);
        }


        private void Subscribe()
        {
            _model.StateChanged += OnStateChanged;
            _view.Clicked += OnClicked;
            _view.CatCaught += OnCatCaught;
            _view.CatMissed += OnCatMissed;
            _view.PlayerCaught += OnPlayerCaught;
            _view.PlayerMissed += OnPlayerMissed;
        }

        private void Unsubscribe()
        {
            _model.StateChanged -= OnStateChanged;
            _view.Clicked -= OnClicked;
            _view.CatCaught -= OnCatCaught;
            _view.CatMissed -= OnCatMissed;
            _view.PlayerCaught -= OnPlayerCaught;
            _view.PlayerMissed -= OnPlayerMissed;
        }


        private void OnStateChanged(TrapState state)
        {
            _view.PlayState(state);

            switch (state)
            {
                case TrapState.Active:
                    Activated?.Invoke();
                    break;

                case TrapState.Completed:
                    Completed?.Invoke();
                    break;
            }
        }

        private void OnClicked()
        {
            if (_model.State == TrapState.Ready)
                _model.SetState(TrapState.Active);
        }

        private void OnCatCaught()
        {
            if (_model.State == TrapState.Idle)
                _model.SetState(TrapState.Ready);
        }

        private void OnCatMissed()
        {
            if (_model.State == TrapState.Ready)
                _model.SetState(TrapState.Idle);
        }

        private void OnPlayerCaught()
        {
            if (_model.State == TrapState.Active)
                _model.SetState(TrapState.Completed);
        }

        private void OnPlayerMissed() { }


        public void Lock()
        {
            if (_model.State != TrapState.Idle)
                throw new InvalidOperationException("Can't lock non-ready trap!");

            _model.SetState(TrapState.Locked);
        }

        public void Unlock()
        {
            if (_model.State != TrapState.Locked)
                throw new InvalidOperationException("Trap is already unlocked!");

            _model.SetState(TrapState.Idle);
        }
    }
}
