using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GameJam.Gameplay.Trap
{
    public class TrapModel
    {
        public event Action<TrapState> StateChanged;

        public readonly string Id;
        public readonly bool IsCritical;
        public readonly float Damage;
        public Sprite Comics;
        public Image ComicsPanel;

        private readonly string[] _unlockingTraps;

        public TrapState State { get; private set; }
        public IReadOnlyCollection<string> UnlockingTraps => _unlockingTraps;


        public TrapModel(TrapConfig trapConfig)
        {
            Comics = trapConfig.Comics;
            ComicsPanel = trapConfig.ComicsPanel;
            Id = trapConfig.Id;
            State = TrapState.Idle;
            Damage = trapConfig.Damage;
            IsCritical = trapConfig.IsCritical;
            _unlockingTraps = trapConfig.UnlockingTraps.ToArray();
        }


        public void SetState(TrapState state)
        {
            if (State == state)
                return;

            State = state;
            StateChanged?.Invoke(state);
        }
    }
}
