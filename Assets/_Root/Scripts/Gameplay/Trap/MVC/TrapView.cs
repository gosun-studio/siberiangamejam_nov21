using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;
using GameJam.Gameplay.Character.Cat;
using GameJam.Gameplay.Character.Player;
using GameJam.Gameplay.Trap.Components;
using GameJam.Gameplay.Components.Catching;

namespace GameJam.Gameplay.Trap
{
    [RequireComponent(typeof(TrapConfig))]
    public class TrapView : ValidatableMonoBehaviour
    {
        [Header("Components")]
        [ValidateInput(nameof(ValidateObject), NullError)]
        [SerializeField] private TrapAnimator _animator = null;

        [ValidateInput(nameof(ValidateObject), NullError)]
        [SerializeField] private CatCatcher _catCatcher = null;

        [ValidateInput(nameof(ValidateObject), NullError)]
        [SerializeField] private PlayerCatcher _playerCatcher = null;

        public event UnityAction Clicked;
        public event UnityAction CatCaught;
        public event UnityAction CatMissed;
        public event UnityAction PlayerCaught;
        public event UnityAction PlayerMissed;


        private void OnValidate()
        {
            _animator ??= GetComponent<TrapAnimator>();
            _catCatcher ??= GetComponent<CatCatcher>();
            _playerCatcher ??= GetComponent<PlayerCatcher>();
        }

        private void OnEnable()
        {
            _catCatcher.Caught += OnCatCaught;
            _catCatcher.Missed += OnCatMissed;
            _playerCatcher.Caught += OnPlayerCaught;
            _playerCatcher.Missed += OnPlayerMissed;
        }

        private void OnDisable()
        {
            _catCatcher.Caught -= OnCatCaught;
            _catCatcher.Missed -= OnCatMissed;
            _playerCatcher.Caught -= OnPlayerCaught;
            _playerCatcher.Missed -= OnPlayerMissed;
        }


        private void OnCatCaught(CatView _) => CatCaught?.Invoke();
        private void OnCatMissed(CatView _) => CatMissed?.Invoke();
        private void OnPlayerCaught(PlayerView _) => PlayerCaught?.Invoke();
        private void OnPlayerMissed(PlayerView _) => PlayerMissed?.Invoke();

        [Button] public void Click() => Clicked?.Invoke();
        public void PlayState(TrapState state) => _animator.PlayState(state);
    }
}
