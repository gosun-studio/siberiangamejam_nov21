using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;

public class ClickableComponent : ValidatableMonoBehaviour
{
    [SerializeField] private UnityEvent _clicked = new UnityEvent();

    public event UnityAction Clicked
    {
        add => _clicked.AddListener(value);
        remove => _clicked.RemoveListener(value);
    }


    private void OnDestroy() =>
        _clicked.RemoveAllListeners();


    [Button]
    public void Click() =>
        _clicked?.Invoke();
}
