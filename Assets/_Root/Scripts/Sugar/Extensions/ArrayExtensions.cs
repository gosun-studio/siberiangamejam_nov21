using System;
using System.Linq;

public static class ArrayExtensions
{
    public static bool Contains<TArray>(this TArray[] source, TArray item) where TArray : class
    {
        for (var i = 0; i < source.Length; i++)
            if (source[i].Equals(item))
                return true;

        return false;
    }

    public static bool TryGetIndexOf<TArray>(this TArray[] source, Func<TArray, bool> condition, out int index) =>
        (index = source.IndexOf(condition)) >= 0;

    public static int IndexOf<TArray>(this TArray[] source, Func<TArray, bool> condition)
    {
        const int defaultResult = -1;

        if (source == null || condition == null)
            return defaultResult;

        for (var i = 0; i < source.Length; i++)
            if (condition(source[i]))
                return i;

        return defaultResult;
    }

    public static bool IsNullOrEmpty<T>(this T[] array) =>
        array.LengthSafe() == 0;

    public static int LengthSafe<T>(this T[] array) =>
        array?.Length ?? 0;

    public static bool TryGetElement<T>(this T[] array, int index, out T element)
    {
        element = default;

        if (index < 0 || index >= array.LengthSafe())
            return false;

        element = array[index];
        return true;
    }

    public static T[] Copy<T>(this T[] array) => array?.ToArray();
}
