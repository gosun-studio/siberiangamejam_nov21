﻿using System;
using System.Linq;
using System.Collections.Generic;

public static class EnumerableExtensions
{
    public static bool IsNullOrEmpty<T>(this IEnumerable<T> source) =>
        source?.Any() != true;

    public static IEnumerable<T> WhereNotNull<T>(this IEnumerable<T> source) =>
        source?.Where(q => q != null) ?? Enumerable.Empty<T>();

    public static IEnumerable<T> Except<T>(this IEnumerable<T> source, Func<T, bool> excepter) =>
        source?.Where(e => !excepter(e)) ?? Enumerable.Empty<T>();
}
