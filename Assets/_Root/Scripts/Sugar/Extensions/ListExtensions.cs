﻿using System.Linq;
using System.Collections.Generic;

public static class ListExtensions
{
    public static void RemoveFirst<T>(this IList<T> list) =>
        list.RemoveAt(0);

    public static void RemoveLast<T>(this IList<T> list) =>
        list.RemoveAt(list.Count - 1);

    public static bool IsNullOrEmpty<T>(this IList<T> list) =>
        list.CountSafe() == 0;

    public static int CountSafe<T>(this IList<T> list) => list?.Count ?? 0;

    public static bool TryGetElement<T>(this IList<T> list, int index, out T element)
    {
        element = default;

        if (index < 0 || index >= list.Count)
            return false;

        element = list[index];
        return true;
    }

    public static List<T> Copy<T>(this List<T> list) => list?.ToList();
}
