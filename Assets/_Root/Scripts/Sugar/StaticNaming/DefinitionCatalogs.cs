public static class DefinitionCatalogs
{
    private const string Root = "Definitions/";

    public const string Ui = Root + "UI/";
    public const string Gameplay = Root + "Gameplay/";
}
