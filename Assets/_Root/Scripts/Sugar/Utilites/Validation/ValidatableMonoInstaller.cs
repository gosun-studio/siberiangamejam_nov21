using Zenject;
using System;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

public class ValidatableMonoInstaller : MonoInstaller
{
    protected const string NullError = Validator.NullError;
    protected const string SameIdsError = Validator.SameIdsError;
    protected const string DuplicatesError = Validator.DuplicatesError;
    protected const string EmptyStringError = Validator.EmptyStringError;

    protected static Color ValidColor => Validator.ValidColor;
    protected static Color InvalidColor => Validator.InvalidColor;

    protected bool isFieldValid = true;
    protected Color FieldColor => isFieldValid ? Validator.ValidColor : Validator.InvalidColor;
    public void ValidateField(bool isFieldValid) => this.isFieldValid = isFieldValid;

    protected bool ValidateObject(Object obj) => Validator.ValidateObject(obj);
    protected bool ValidateTObject<T>(T obj, Func<T, bool> condition) => Validator.ValidateTObject(obj, condition);

    protected bool ValidateObjects(IEnumerable<Object> objs) => Validator.ValidateObjects(objs);
    protected bool ValidateTObjects<T>(IEnumerable<T> objs, Func<T, bool> condition) => Validator.ValidateTObjects(objs, condition);

    protected bool ValidateDuplicates(IEnumerable<Object> objs) => Validator.ValidateDuplicates(objs);
    protected bool ValidateTDuplicates<T>(IEnumerable<T> objs) => Validator.ValidateTDuplicates(objs);
    protected bool ValidateStringDuplicates(IEnumerable<string> strs) => Validator.ValidateStringDuplicates(strs);

    protected void Error(string message) => Validator.Error(message);
}
