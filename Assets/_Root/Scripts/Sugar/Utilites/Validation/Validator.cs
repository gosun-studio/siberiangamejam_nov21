﻿using System;
using UnityEngine;
using System.Linq;
using System.Collections.Generic;

using Object = UnityEngine.Object;

public static class Validator
{
    public const string NullError = "Can't be null!";
    public const string SameIdsError = "Has same ids!";
    public const string DuplicatesError = "Has duplicates!";
    public const string EmptyStringError = "Can't be empty!";

    public static readonly Color ValidColor = new Color(1f, 1f, 1f, 1f);
    public static readonly Color InvalidColor = new Color(1f, 0.4f, 0.4f, 1f);

    public static bool ValidateObject(Object obj) => obj != null;
    public static bool ValidateTObject<T>(T obj, Func<T, bool> condition) => condition(obj);

    public static bool ValidateObjects(IEnumerable<Object> objs) => ValidateTObjects(objs, ValidateObject);

    public static bool ValidateTObjects<T>(IEnumerable<T> objs, Func<T, bool> condition) =>
        objs.IsNullOrEmpty() || objs.All(condition);

    public static bool ValidateDuplicates(IEnumerable<Object> objs) => ValidateTDuplicates(objs);
    public static bool ValidateTDuplicates<T>(IEnumerable<T> objs) => objs.Count() == objs.Distinct().Count();
    public static bool ValidateStringDuplicates(IEnumerable<string> strs) => ValidateTDuplicates(strs);

    public static void Error(string message) => Debug.LogError(message);
}
