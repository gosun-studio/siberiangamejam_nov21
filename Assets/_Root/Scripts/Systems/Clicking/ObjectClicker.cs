﻿using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.Events;
using GameJam.Systems.PlayerInput;
using GameJam.Systems.Raycasting;

namespace GameJam.Systems.Clicking
{
    public class ObjectClicker : ValidatableMonoBehaviour
    {
        [Header("Components")]
        [ValidateInput(nameof(ValidateObject), NullError)]
        [SerializeField] private InputNotifier _inputNotifier = null;

        [ValidateInput(nameof(ValidateObject), NullError)]
        [SerializeField] private ScreenRaycaster _screenRaycaster = null;

        [Header("Params")]
        [Tooltip("Смещение, допустимое для клика, между позицией начала клика и конца клика")]
        [SerializeField, Min(0)] private float _clickError = 3f;

        [Header("Events")]
        [SerializeField] private UnityEvent<RaycastHit> _clicked = new UnityEvent<RaycastHit>();

        public event UnityAction<RaycastHit> Clicked
        {
            add => _clicked.AddListener(value);
            remove => _clicked.RemoveListener(value);
        }

        private GameObject _gameObjectCache;
        private Vector2? _pointerPressedPosition;

        private Vector2 PointerPosition => _inputNotifier.PointerPosition;
        private bool IsActive => enabled && (_gameObjectCache ??= gameObject).activeInHierarchy;


        private void OnValidate()
        {
            _inputNotifier ??= GetComponent<InputNotifier>();
            _screenRaycaster ??= GetComponent<ScreenRaycaster>();
        }

        private void OnEnable()
        {
            _inputNotifier.PointerUp += OnPointerUp;
            _inputNotifier.PointerDown += OnPointerDown;
        }

        private void OnDisable()
        {
            ResetClick();
            _inputNotifier.PointerUp -= OnPointerUp;
            _inputNotifier.PointerDown -= OnPointerDown;
        }


        private void OnPointerDown()
        {
            if (IsActive)
                _pointerPressedPosition = PointerPosition;
        }

        private void OnPointerUp()
        {
            if (!IsActive || !_pointerPressedPosition.HasValue)
                return;

            const int squaredPower = 2;
            Vector2 pointerReleasedPosition = PointerPosition;
            float squaredDistance = Vector3.SqrMagnitude(pointerReleasedPosition - _pointerPressedPosition.Value);
            float squaredError = Mathf.Pow(_clickError, squaredPower);

            if (squaredDistance > squaredError)
                return;

            if (_screenRaycaster.TryRaycast(pointerReleasedPosition, out var hit))
                _clicked?.Invoke(hit);

            ResetClick();
        }

        private void ResetClick() =>
            _pointerPressedPosition = null;
    }
}
