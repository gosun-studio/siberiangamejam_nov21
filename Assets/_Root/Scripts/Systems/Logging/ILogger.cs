namespace GameJam.Systems.Logging
{
    public interface ILogger
    {
        void Log(string message);
        void Error(string message);
        void Warning(string message);
    }
}