using UnityEngine;

namespace GameJam.Systems.Logging
{
    public class LoggerComponent : ValidatableMonoBehaviour, ILogger
    {
        [SerializeField] private string _componentName = "";

        private ILogger _logger;
        private ILogger _loggerForEnabled;
        private ILogger _loggerForDisabled;


        private void Awake()
        {
            _loggerForDisabled = StubLogger.Instance;
            _loggerForEnabled = new ObjectLogger(_componentName);
        }

        private void OnEnable() => _logger = _loggerForEnabled;
        private void OnDisable() => _logger = _loggerForDisabled;

        public void Log(string message) => _logger.Log(message);
        public new void Error(string message) => _logger.Error(message);
        public void Warning(string message) => _logger.Warning(message);
    }
}