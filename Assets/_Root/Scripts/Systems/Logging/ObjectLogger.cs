using UnityEngine;

namespace GameJam.Systems.Logging
{
    public class ObjectLogger : ILogger
    {
        private readonly string _objectType;

        public ObjectLogger(string objectType) =>
            _objectType = objectType;


        public void Log(string message)
        {
            message = WrapMessage(message);
            Debug.Log(message);
        }

        public void Error(string message)
        {
            message = WrapMessage(message);
            Debug.LogError(message);
        }

        public void Warning(string message)
        {
            message = WrapMessage(message);
            Debug.LogWarning(message);
        }


        private string WrapMessage(string message) =>
            $"[{_objectType}] {message}";
    }
}