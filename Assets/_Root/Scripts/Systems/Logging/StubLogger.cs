namespace GameJam.Systems.Logging
{
    public class StubLogger : ILogger
    {
        public static readonly StubLogger Instance = new StubLogger();

        public void Log(string message)
        { }

        public void Error(string message)
        { }

        public void Warning(string message)
        { }
    }
}