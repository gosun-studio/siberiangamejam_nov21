﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace GameJam.Systems.PlayerInput
{
    public class InputNotifier : ValidatableMonoBehaviour, IDisposable
    {
        private const int PointerMouseButton = 0;

        [Header("Events for pointer")]
        [SerializeField] private UnityEvent _pointerUp = new UnityEvent();
        [SerializeField] private UnityEvent _pointerDown = new UnityEvent();

        [Header("Events for keys")]
        [SerializeField] private List<KeyEvents> _keyEvents = new List<KeyEvents>();

        public event UnityAction<KeyCode> KeyUp;
        public event UnityAction<KeyCode> KeyDown;
        public event UnityAction PointerUp
        {
            add => _pointerUp.AddListener(value);
            remove => _pointerUp.RemoveListener(value);
        }
        public event UnityAction PointerDown
        {
            add => _pointerDown.AddListener(value);
            remove => _pointerDown.RemoveListener(value);
        }

        public IReadOnlyCollection<KeyEvents> KeyEvents => _keyEvents;
        public Vector2 PointerPosition => Input.mousePosition;

        private bool _isDisposed;


        public void Dispose()
        {
            if (_isDisposed)
                return;

            _isDisposed = true;

            _pointerUp.RemoveAllListeners();
            _pointerDown.RemoveAllListeners();

            _pointerUp = null;
            _pointerDown = null;

            foreach (KeyEvents keyEvent in _keyEvents)
                keyEvent.Dispose();

            _keyEvents.Clear();
        }

        private void OnDestroy() => Dispose();

        private void Update()
        {
            CheckPointerEvents();
            CheckKeyEvents();
        }

        private void CheckPointerEvents()
        {
            if (Input.GetMouseButtonDown(PointerMouseButton))
                _pointerDown?.Invoke();

            if (Input.GetMouseButtonUp(PointerMouseButton))
                _pointerUp?.Invoke();
        }

        private void CheckKeyEvents()
        {
            for (int i = 0; i < _keyEvents.CountSafe(); i++)
            {
                KeyEvents keyEvent = _keyEvents[i];

                if (Input.GetKeyDown(keyEvent.KeyCode))
                {
                    keyEvent.KeyDown?.Invoke();
                    KeyDown?.Invoke(keyEvent.KeyCode);
                }

                if (Input.GetKeyUp(keyEvent.KeyCode))
                {
                    keyEvent.KeyUp?.Invoke();
                    KeyUp?.Invoke(keyEvent.KeyCode);
                }
            }
        }


        public bool ContainsKey(KeyCode keyCode) =>
            _keyEvents.Exists(GetSearchPredicate(keyCode));


        public bool TryGetKeyEvents(KeyCode keyCode, out KeyEvents keyEvents) =>
            (keyEvents = GetKeyEvents(keyCode)) != null;

        public KeyEvents GetKeyEvents(KeyCode keyCode)
        {
            bool contains = TryGetIndexOf(keyCode, out var index);
            return contains ? _keyEvents[index] : null;
        }


        public KeyEvents AddKeyEvents(KeyCode keyCode)
        {
            bool contains = TryGetIndexOf(keyCode, out var index);
            KeyEvents result = contains ? _keyEvents[index] : new KeyEvents(keyCode);

            if (!contains)
                _keyEvents.Add(result);

            return result;
        }

        public void RemoveKeyEvents(KeyCode keyCode)
        {
            bool contains = TryGetIndexOf(keyCode, out var index);
            if (contains)
                _keyEvents.RemoveAt(index);
        }


        private bool TryGetIndexOf(KeyCode keyCode, out int index) =>
            (index = _keyEvents.FindIndex(GetSearchPredicate(keyCode))) >= 0;

        private Predicate<KeyEvents> GetSearchPredicate(KeyCode keyCode) =>
            k => k.KeyCode == keyCode;
    }
}
