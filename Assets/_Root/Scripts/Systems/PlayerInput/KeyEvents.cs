﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace GameJam.Systems.PlayerInput
{
    [Serializable]
    public class KeyEvents : IDisposable
    {
        [field: SerializeField] public KeyCode KeyCode { get; private set; } = KeyCode.Escape;
        [field: SerializeField] public UnityEvent KeyUp { get; private set; } = new UnityEvent();
        [field: SerializeField] public UnityEvent KeyDown { get; private set; } = new UnityEvent();

        private bool _isDisposed;


        public KeyEvents(KeyCode keyCode)
        {
            KeyCode = keyCode;
            KeyUp = new UnityEvent();
            KeyDown = new UnityEvent();
        }

        public void Dispose()
        {
            if (_isDisposed)
                return;

            _isDisposed = true;

            KeyUp.RemoveAllListeners();
            KeyDown.RemoveAllListeners();

            KeyUp = null;
            KeyDown = null;
        }
    }
}
