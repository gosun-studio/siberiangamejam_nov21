﻿using UnityEngine;
using Sirenix.OdinInspector;

namespace GameJam.Systems.Raycasting
{
    public class ScreenRaycaster : ValidatableMonoBehaviour
    {
        [Header("Components")]
        [ValidateInput(nameof(ValidateObject), NullError)]
        [SerializeField] private Camera _camera;

        [Header("Params")]
        [SerializeField] private LayerMask _raycastLayerMask;
        [SerializeField, Min(0)] private float _maxRaycastDistance = Mathf.Infinity;


        public bool TryRaycast(Vector2 screenPoint, out RaycastHit hit)
        {
            Ray ray = _camera.ScreenPointToRay(screenPoint);
            return Physics.Raycast(ray, out hit, _maxRaycastDistance, _raycastLayerMask);
        }

        public RaycastHit? Raycast(Vector2 screenPoint) =>
            TryRaycast(screenPoint, out var hit) ? (RaycastHit?)hit : null;
    }
}
