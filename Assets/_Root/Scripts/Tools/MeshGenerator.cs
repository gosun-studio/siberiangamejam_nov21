using System;
using System.Linq;
using UnityEngine;
using Sirenix.OdinInspector;

namespace GameJam.Tools
{
    [RequireComponent(typeof(MeshFilter))]
    public class MeshGenerator : ValidatableMonoBehaviour
    {
        [ValidateInput(nameof(ValidateObject), NullError)]
        [SerializeField] private MeshFilter _meshFilter = new MeshFilter();

        [SerializeField] private Mesh _mesh = null;
        [SerializeField] private Transform[] _vertices = Array.Empty<Transform>();
        [SerializeField] private int[] _triangles = Array.Empty<int>();


        private void Start() => UpdateMesh();
        private void InitMeshFilter() => _meshFilter ??= GetComponent<MeshFilter>();

        [Button]
        private void UpdateMesh()
        {
            _mesh ??= new Mesh();
            _mesh.Clear();

            _mesh.vertices = _vertices.Select(v => v.position).ToArray();
            _mesh.triangles = _triangles;

            _mesh.RecalculateNormals();

            InitMeshFilter();
            _meshFilter.mesh = _mesh;
        }
    }
}
