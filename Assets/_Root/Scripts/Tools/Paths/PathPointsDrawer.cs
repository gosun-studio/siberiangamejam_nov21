using System;
using System.Linq;
using GameJam.Gameplay.Paths;
using UnityEngine;
using Sirenix.OdinInspector;

namespace GameJam.Tools.Paths
{
    [ExecuteInEditMode]
    public class PathPointsDrawer : ValidatableMonoBehaviour
    {
        [SerializeField] private Color _color = Color.magenta;
        [SerializeField, MinValue(0)] private float _sphereRadius = 0.5f;

        private Transform[] _pathPoints;


        private void OnEnable() =>
            _pathPoints = FindObjectsOfType<PathPointConfig>()
                .Select(pp => pp.transform)
                .ToArray();

        private void OnDisable() =>
            _pathPoints = Array.Empty<Transform>();

        private void OnDrawGizmos()
        {
            Gizmos.color = _color;

            foreach (Transform pathPoint in _pathPoints)
                Gizmos.DrawSphere(pathPoint.position, _sphereRadius);
        }
    }
}
