using System;
using UnityEngine;
using GameJam.Gameplay.Paths;
using System.Collections.Generic;

namespace GameJam.Tools.Paths
{
    [ExecuteInEditMode]
    public class PathsDrawer : ValidatableMonoBehaviour
    {
        [SerializeField] private Color _color = Color.magenta;

        private PathConfig[] _paths;


        private void OnEnable() => _paths = FindObjectsOfType<PathConfig>();
        private void OnDisable() => _paths = Array.Empty<PathConfig>();

        private void OnDrawGizmos()
        {
            Gizmos.color = _color;
            foreach (PathConfig path in _paths)
                DrawPath(path);
        }


        private void DrawPath(PathConfig path)
        {
            IReadOnlyList<PathPointConfig> points = path.Points;

            DrawFinitePath(points);

            if (path.Looped)
                DrawToLoop(points);
        }

        private void DrawFinitePath(IReadOnlyList<PathPointConfig> points)
        {
            const int minPoints = 2;
            const int initialIndex = 0;

            if (points.Count < minPoints)
                return;

            PathPointConfig lastPoint = points[initialIndex];
            for (int i = initialIndex + 1; i < points.Count; i++)
            {
                PathPointConfig point = points[i];
                Gizmos.DrawLine(lastPoint.transform.position, point.transform.position);

                lastPoint = point;
            }
        }

        private void DrawToLoop(IReadOnlyList<PathPointConfig> points)
        {
            const int minPoints = 3;
            const int startIndex = 0;
            int endIndex = points.Count - 1;

            if (points.Count < minPoints)
                return;

            PathPointConfig startPoint = points[startIndex];
            PathPointConfig endPoint = points[endIndex];

            Gizmos.DrawLine(endPoint.transform.position, startPoint.transform.position);
        }
    }
}
