using UnityEngine;
using GameJam.Gameplay.Trap;

namespace GameJam.Tools.Traps
{
    public readonly struct GizmosLine
    {
        private readonly TrapConfig _startTrap;
        private readonly TrapConfig _endTrap;

        public Vector3 Start => _startTrap.transform.position;
        public Vector3 End => _endTrap.transform.position;


        public GizmosLine(TrapConfig start, TrapConfig end)
        {
            _startTrap = start;
            _endTrap = end;
        }
    }
}
