using UnityEngine;
using System.Linq;
using System.Collections.Generic;
using GameJam.Gameplay.Trap;
using Sirenix.OdinInspector;

namespace GameJam.Tools.Traps
{
    [ExecuteInEditMode]
    public class TrapConnectionsDrawer : MonoBehaviour
    {
        [SerializeField] private Color _color = Color.green;
        [SerializeField, MinValue(0)] private float _endSphereRadius = 0.7f;

        private Dictionary<string, TrapConfig> _trapConfigs;
        private List<GizmosLine> _lines;


        private void OnEnable()
        {
            _trapConfigs = InitConfigs();
            _lines = InitLines(_trapConfigs);
        }

        private void OnDisable()
        {
            _trapConfigs.Clear();
            _lines.Clear();
        }

        private void OnDrawGizmos()
        {
            if (!enabled)
                return;

            Gizmos.color = _color;
            foreach (GizmosLine connection in _lines)
            {
                Gizmos.DrawLine(connection.Start, connection.End);
                Gizmos.DrawSphere(connection.End, _endSphereRadius);
            }
        }


        private Dictionary<string, TrapConfig> InitConfigs() =>
            FindObjectsOfType<TrapConfig>()
                .ToDictionary(tc => tc.Id, tc => tc);

        private List<GizmosLine> InitLines(Dictionary<string, TrapConfig> configs)
        {
            var list = new List<GizmosLine>();

            foreach (TrapConfig config in configs.Values)
            foreach (string unlockingId in config.UnlockingTraps)
            {
                TrapConfig unlockingConfig = configs[unlockingId];
                GizmosLine line = new GizmosLine(config, unlockingConfig);
                list.Add(line);
            }

            return list;
        }
    }
}
