using UnityEngine;
using System.Reflection;
using Sirenix.OdinInspector;
using GameJam.Gameplay.Trap;

namespace GameJam.Tools.Traps
{
    [ExecuteInEditMode]
    public class TrapIdGenerator : MonoBehaviour
    {
        [SerializeField] private string _idFieldName = "_id";
        [SerializeField] private string _idMask = "trap_{0}";


        [Button(ButtonSizes.Large)]
        public void GenerateTrapIds()
        {
            TrapConfig[] trapConfigs = FindObjectsOfType<TrapConfig>();

            for (int i = 0; i < trapConfigs.Length; i++)
                SetId
                (
                    trapConfigs[i],
                    GenerateId(i)
                );
        }

        private string GenerateId(int index) =>
            _idMask.IsNullOrEmpty() ?
                index.ToString() :
                string.Format(_idMask, index);

        private void SetId(TrapConfig trapConfig, string id)
        {
            FieldInfo idField = GetPrivateField(trapConfig, _idFieldName);
            idField.SetValue(trapConfig, id);
        }

        private FieldInfo GetPrivateField(TrapConfig trapConfig, string fieldName) =>
            trapConfig.GetType().GetField(fieldName,
                BindingFlags.NonPublic | BindingFlags.Instance);
    }
}
