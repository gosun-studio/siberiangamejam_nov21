using System;
using System.Linq;
using GameJam.Gameplay.Trap;
using Sirenix.OdinInspector;
using UnityEngine;

namespace GameJam.Tools.Traps
{
    [ExecuteInEditMode]
    public class TrapsDrawer : ValidatableMonoBehaviour
    {
        [SerializeField] private Color _color = Color.blue;
        [SerializeField, MinValue(0)] private float _sphereRadius = 0.4f;

        private Transform[] _traps;


        private void OnEnable() =>
            _traps = FindObjectsOfType<TrapConfig>()
                .Select(pp => pp.transform)
                .ToArray();

        private void OnDisable() =>
            _traps = Array.Empty<Transform>();

        private void OnDrawGizmos()
        {
            Gizmos.color = _color;

            foreach (Transform trap in _traps)
                Gizmos.DrawSphere(trap.position, _sphereRadius);
        }
    }
}
