﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;

namespace GameJam.UI
{
    public class ButtonFuctions : MonoBehaviour
    {
        [SerializeField]
        private GameObject background; 
        private bool menuActive = false;
        private bool checkOpenedWindow = false;
        [SerializeField] private GameObject menu;
        public void ButtonExit(GameObject closeObj)
        {
            closeObj.SetActive(false);
        }
        public void ButtonExit(string sceneName)
        {
            Time.timeScale = 1;
            SceneManager.LoadScene(sceneName);
        }

        public void ButtonCloseSettings(GameObject settings)
        {
            settings.SetActive(false);
            checkOpenedWindow = false;
        }
        public void ButtonExit()
        {
            Application.Quit();
        }
        public void ButtonSettings(GameObject obj)
        {
            checkOpenedWindow = true;
            obj.SetActive(true);
        }

        public void DestroyDialog()
        {
            Destroy(gameObject);
        }
        
        void Update()
        {
            if (SceneManager.GetActiveScene().name == "Level" && Input.GetKeyUp(KeyCode.Escape))
            {
                if (!menuActive)
                {
                    background.SetActive(true);
                    Time.timeScale = 0;
                    menu.SetActive(true);
                    menuActive = true;
                }
                
                else if (menuActive && !checkOpenedWindow)
                {
                    background.SetActive(false);
                    Time.timeScale = 1;
                    menu.SetActive(false);
                    menuActive = false;
                }
            }
        }
    }
}