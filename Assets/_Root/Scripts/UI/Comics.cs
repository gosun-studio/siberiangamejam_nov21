using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Comics : MonoBehaviour
{
    public List<Sprite> Comics1;
    public GameObject Parent;

    public void StartAnim()
    {
        Parent.SetActive(true);
        StartCoroutine(StartComics());
    }

    IEnumerator StartComics()
    {
        foreach (var sprite in Comics1)
        {
            gameObject.GetComponent<Image>().sprite = sprite;
            yield return new WaitForSeconds(1);
            gameObject.GetComponent<Image>().DOFade(1, 1);
            yield return new WaitForSeconds(2);
            gameObject.GetComponent<Image>().DOFade(0, 1); 
            yield return new WaitForSeconds(1);
        }
        SceneManager.LoadScene("Level");
    }
}
