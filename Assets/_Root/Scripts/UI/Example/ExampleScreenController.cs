namespace GameJam.UI
{
    public class ExampleScreenController : ScreenController<ExampleScreenView>
    {
        public ExampleScreenController(ExampleScreenView view) : base(view)
        { }
    }
}
