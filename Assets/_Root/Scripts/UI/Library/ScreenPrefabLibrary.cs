using System;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;

namespace GameJam.UI
{
    [CreateAssetMenu(fileName = nameof(ScreenPrefabLibrary), menuName = DefinitionCatalogs.Ui + nameof(ScreenPrefabLibrary))]
    public class ScreenPrefabLibrary : ValidatableScriptableObject
    {
        [ValidateInput(nameof(ValidateDuplicateTypes), DuplicatesError)]
        [SerializeField] private ScreenPrefabLibraryData[] _prefabVariants = Array.Empty<ScreenPrefabLibraryData>();

        public IReadOnlyCollection<ScreenPrefabLibraryData> PrefabVariants => _prefabVariants;


        private bool ValidateDuplicateTypes() =>
            ValidateTDuplicates(_prefabVariants.Select(p => p.Type));
    }
}
