using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace GameJam.UI
{
    [Serializable]
    public class ScreenPrefabLibraryData : ValidatableClass
    {
        [field: SerializeField] public ScreenType Type { get; private set; }

        [field: ValidateInput(nameof(ValidateObject), NullError)]
        [field: SerializeField] public GameObject Screen { get; private set; }
    }
}
