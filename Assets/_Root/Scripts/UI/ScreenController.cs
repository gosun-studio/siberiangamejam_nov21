namespace GameJam.UI
{
    public interface IScreenController
    {
        void Close();
    }

    public abstract class ScreenController<TView> : IScreenController
        where TView : ScreenView
    {
        private readonly TView _view;


        protected ScreenController(TView view)
        {
            _view = view;
        }

        public void Close() { }
    }
}
