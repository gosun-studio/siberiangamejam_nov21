using System;

namespace GameJam.UI
{
    public class ScreenFactory
    {
        private readonly ScreenPrefabRepository _prefabRepository;


        public ScreenFactory(ScreenPrefabRepository prefabRepository)
        {
            _prefabRepository = prefabRepository;
        }


        public IScreenController CreateScreen<TController>(ScreenType type)
        {
            switch (type)
            {
                case ScreenType.Example:
                    var view = _prefabRepository.GetPrefab(type).GetComponent<ExampleScreenView>();
                    var controller = new ExampleScreenController(view);
                    return (IScreenController)controller;

                default:
                    throw new ArgumentOutOfRangeException($"Invalid {typeof(ScreenType)}");
            }
        }
    }
}
