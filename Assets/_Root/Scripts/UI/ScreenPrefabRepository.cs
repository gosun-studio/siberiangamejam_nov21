using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace GameJam.UI
{
    public class ScreenPrefabRepository
    {
        private readonly Dictionary<ScreenType, GameObject> _prefabs;


        public ScreenPrefabRepository(IReadOnlyCollection<ScreenPrefabLibraryData> data) =>
            _prefabs = ConvertToDictionary(data);

        private Dictionary<ScreenType, GameObject> ConvertToDictionary(IReadOnlyCollection<ScreenPrefabLibraryData> data) =>
            data.ToDictionary(d => d.Type, d => d.Screen);


        public bool Contains(ScreenType type) => _prefabs.ContainsKey(type);

        public GameObject GetPrefab(ScreenType type) => _prefabs[type];

        public bool TryGetPrefab(ScreenType type, out GameObject prefab) => _prefabs.TryGetValue(type, out prefab);
    }
}
