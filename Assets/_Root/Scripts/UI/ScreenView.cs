namespace GameJam.UI
{
    public interface IScreenView
    {

    }

    public abstract class ScreenView : ValidatableMonoBehaviour, IScreenView
    {

    }
}
